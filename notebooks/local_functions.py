__all__ = ["transformed_box", "create_cylinder_phantom", 
           "create_spherical_phantom", "add_custom_axes", 
           "add_refilling_box", "animate_trajectories", 
           "get_custom_theme", "map_susceptibility_to_xcat_phantom"]
from typing import Sequence

import pyvista 
import numpy as np
from pint import Quantity
from tqdm.notebook import tqdm


def transformed_box(reference_basis: np.ndarray, slice_normal: np.ndarray,
                    readout_direction: np.ndarray, slice_position: Quantity,
                    extend: Quantity = Quantity([30, 30, 1], "cm")):
    """
    
    :param reference_basis: (3x3) matrix where row-index reference the x, y, z
                            basis vectors
    
    """
    from scipy.spatial.transform import Rotation
    slice_normal /= np.linalg.norm(slice_normal)

    if readout_direction is None:
        if np.allclose(slice_normal, np.array([1., 0., 0.]), atol=1e-3):
            readout_direction = np.array([0., 1., 0.])
        else:
            readout_direction = np.array([1., 0., 0.])
    else:

        if np.isclose(
                np.dot(slice_normal, readout_direction / np.linalg.norm(readout_direction)),
                1., atol=1e-3):
            raise ValueError("Slice normal and readout-direction are parallel")

    readout_direction = readout_direction - np.dot(slice_normal,
                                                   readout_direction) * slice_normal
    readout_direction /= np.linalg.norm(readout_direction)
    pe_direction = np.cross(readout_direction, slice_normal)
    pe_direction /= np.linalg.norm(pe_direction)
    new_basis = np.stack([readout_direction, pe_direction, slice_normal])
    rot, _ = Rotation.align_vectors(reference_basis, new_basis)

    total_transform = np.eye(4, 4)
    total_transform[:3, :3] = rot.as_matrix()
    total_transform[:3, 3] = slice_position.m_as("m")
    bounds = np.stack([-extend.m_as("m")/2, extend.m_as("m")/2], axis=1).reshape(-1)    
    
    box = pyvista.Box(bounds=bounds)
    box.transform(total_transform, inplace=True)
    return box


def create_cylinder_phantom(dimensions=(200, 200, 4), spacing=(0.001, 0.001, 0.0025), 
                            radii=Quantity((4, 3, 2), "cm"),
                            xy_positions=Quantity([[-3, -3], [0, 5], [5, 0]], "cm"),
                            m0=np.array([0.5, 0.8, 0.9, 0.8]),
                            t1=Quantity([1000, 800, 900, 800], "ms"),
                            t2=Quantity([200, 250, 250, 250], "ms"),
                            t2star=Quantity([50, 50, 50, 50], "ms")) -> pyvista.PolyData:
    """ Creates a phantom with N  holes on the coordinate axes and
     a rectangular hole in the center.

    :param dimensions: (int, int, int)
    :param spacing: (float, float, float) in meter
    :param big_radius: float in meter
    :return: pyvista.UnstructuredGrid
    """

    raster = pyvista.ImageData(dimensions=dimensions, spacing=spacing, origin=-np.array(dimensions) * np.array(spacing) / 2)
    cylinders = [pyvista.Cylinder(center=(*xy_positions[i].m_as("m"), 0.), direction=(0., 0., 1.), radius=radii[i].m_as("m"),)
                 for i in range(len(radii))]
    max_radius = np.max(np.array(dimensions[0:2]) * np.array(spacing[0:2])) / 2

    background = pyvista.Cylinder(center=(0., 0., 0.), direction=(0., 0., 1.), radius=max_radius)
    background = raster.clip_surface(background, progress_bar=True, invert=True)

    for cyl in cylinders:
        background = background.clip_surface(cyl, progress_bar=True, invert=False)
    background["class"] = np.ones(background.points.shape[0])

    voxelized_meshes = [raster.clip_surface(sms, progress_bar=True, invert=True) for sms in cylinders]
    for idx, m in enumerate(voxelized_meshes):
        m["class"] = np.ones(m.points.shape[0], dtype=np.int64) * (idx + 2)

    res = pyvista.PolyData(np.concatenate([background.points, *[v.points for v in voxelized_meshes]]))
    res["class"] = np.concatenate([background["class"], *[v["class"] for v in voxelized_meshes]])

    res = raster.interpolate(res, radius=0.001)
    res["class"] = np.around(res["class"], decimals=0).astype(np.int32) - 1

    for k in ["M0", "T1", "T2", "T2star"]:
        res[k] = np.zeros(res.points.shape[0])

    class_indices = [np.where(res["class"] == idx) for idx in range(len(radii) + 1)]
    for cidx, m0_, t1_, t2_, t2star_ in zip(class_indices, m0, t1.m_as("ms"), t2.m_as("ms"), t2star.m_as("ms")):
        res["M0"][cidx] = m0_; res["T1"][cidx] = t1_; res["T2"][cidx] = t2_; res["T2star"][cidx] = t2star_;

    return res.threshold(0., "class", preference="point")


def create_spherical_phantom(dimensions=(120, 120, 120), spacing=(0.002, 0.002, 0.002),
                             big_radius=0.1) -> pyvista.UnstructuredGrid:
    """ Creates a spherical phantom with 6 spherical holes on the coordinate axes and
     a rectangular hole in the center.

    :param dimensions: (int, int, int)
    :param spacing: (float, float, float) in meter
    :param big_radius: float in meter
    :return: pyvista.UnstructuredGrid
    """
    raster = pyvista.ImageData(dimensions=dimensions, spacing=spacing,
                                 origin=-np.array(dimensions) * np.array(spacing) / 2)
    sphere_big = pyvista.Sphere(radius=big_radius).extract_surface()
    box = pyvista.Box(bounds=(-big_radius * 0.3, big_radius * 0.3, -big_radius/5,
                              big_radius/5, -big_radius/10, big_radius/10))

    o = big_radius * 0.7
    spheres_small = [pyvista.Sphere(radius=big_radius*0.05, center=c).extract_surface() for c in
                     ((-o, 0., 0.), (o, 0., 0.), (0., -o, 0.), (0., o, 0.), (0., 0., -o),
                      (0., 0., o))]
    del o

    filled_sphere = raster.clip_surface(sphere_big, progress_bar=True)
    filled_sphere = filled_sphere.clip_surface(box, progress_bar=True, invert=False)
    for sms in spheres_small:
        filled_sphere = filled_sphere.clip_surface(sms, progress_bar=True, invert=False)
    return filled_sphere


def add_custom_axes(plotter: pyvista.Plotter, scale: float = 0.1):
    """ Add scaled arrows to the plotter at origin  (x,y,z) -> (blue, green, red)
    
    :param plotter:
    :param scale:
    """
    origin_arrows = [pyvista.Arrow(start=(0., 0., 0.), direction=direction, tip_length=0.1,
                                   tip_radius=0.025, tip_resolution=10, shaft_radius=0.005,
                                   shaft_resolution=10, scale=scale)
                     for direction in [(1., 0., 0.), (0., 1., 0.), (0., 0., 1.)]]
    plotter.add_mesh(origin_arrows[0], color="blue", label="x")
    plotter.add_mesh(origin_arrows[1], color="green", label="y")
    plotter.add_mesh(origin_arrows[2], color="red", label="z")
    
    
def add_refilling_box(plotter: pyvista.Plotter, bounding_box: Quantity,
                      thickness: Quantity, position: Quantity):
    """ Adds an orange box oriented in (x,y,z) to the plotter, to illustrate a reseeding volume

    :param plotter: -
    :param bounding_box: (2, )
    :param thickness: ( )
    :param position: (3, )
    :return:
    """
    slice_mesh = pyvista.Box(bounds=(-bounding_box[0].m_as("m")/2, bounding_box[0].m_as("m")/2,
                                     -bounding_box[1].m_as("m")/2, bounding_box[1].m_as("m") /2,
                                     -thickness.m_as("m")/2, thickness.m_as("m")/2))
    slice_mesh.translate(position.m_as("m"), inplace=True)
    plotter.add_mesh(slice_mesh, color="orange", opacity=0.5, show_edges=True)
    
    
def animate_trajectories(plotter: pyvista.Plotter,
                         trajectories: Sequence,
                         timing: np.ndarray, filename: str, scalars: Sequence = None,
                         initial_mesh: pyvista.UnstructuredGrid = None,
                         vectors: Sequence = None,
                         multi_vecs: bool = False,
                         vector_pos: Sequence = None,
                         mesh_kwargs: dict = None,
                         text_kwargs: dict = None,
                         vector_kwargs: dict = None,
                         visualize_trajectories: bool = False,
                         visualize_trajectories_max_length: int = None,
                         trajectory_kwargs: dict = None,
                         projection_curve: np.ndarray = None,
                         max_projection_curve_len: int = None,
                         projection_kwargs: dict = None):
    """Animates Sequence of trajectories and saves the animation as gif

    :param plotter:
    :param trajectories: sequence containing positions of arbitrary points. Each entry corresponds
            to a single snapshot
    :param timing: sequence of time-values corresponding to the snapshots in trajectories
    :param filename: full path of the saved file (must include .gif ending)
    :param scalars: scalar values corresponding to each entry of trajectories (shapes must match)
    :param vectors: vectors corresponding to each entry of vector pos (shapes must match)
    :param multi_vecs: bool - if true it is assumed that each entry of 'vectors' argument contains
                    multiple vectors per time-step / per point
    :param vector_pos: positions of vectors specified in argument 'vectors'
    :param mesh_kwargs: dictionary containing arguments to plotter.add_mesh(.., **mesh_kwargs)
    :param text_kwargs: dictionary containing arguments to plotter.add_text(..., **text_kwargs)
    :param vector_kwargs: dictionary containing arguments to plotter.add_arrows(..., **vector_kwargs)
    :param visualize_trajectories: if true, particle trajectories are traced with a line
    :param trajectory_kwargs: dictionary containing argument for lines tracing the trajectories
    """

    if mesh_kwargs is None:
        mesh_kwargs = dict()

    if text_kwargs is None:
        text_kwargs = dict(color='white', shadow=False, font_size=26,
                           position='upper_right')
    if vector_kwargs is None:
        vector_kwargs = dict(mag=1e-3)

    if vector_pos is None:
        vector_pos = trajectories

    if trajectory_kwargs is None:
        trajectory_kwargs = dict(color="red", show_scalar_bar=False)

    if scalars is not None:
        assert len(scalars) == len(trajectories)

    if projection_curve is not None:
        assert len(projection_curve) == len(trajectories)
    
    plotter.open_gif(f"{filename}")
    if timing is not None:
        desc = f"Rendering Mesh-Animation from {timing[0]:2.3f}ms to {timing[-1]:2.3f} ms"

    # This initialization only 
    if initial_mesh is None:
        plot_mesh = pyvista.PolyData(trajectories[0])
    else:
        plot_mesh = initial_mesh
    
    if visualize_trajectories_max_length is None:
        visualize_trajectories_max_length = len(trajectories)
        
    if max_projection_curve_len is None and projection_curve is not None:
        max_projection_curve_len = len(projection_curve)
    if projection_kwargs is None:
        projection_kwargs = {}
    
    trace_glyphs = []
    projection_glyphs = []
    pbar = tqdm(range(len(trajectories)), desc=desc, leave=False)
    for step in pbar: 
        if step > 0:
            if trajectories[step].shape[0] == trajectories[step-1].shape[0]:
                plot_mesh.points = trajectories[step]
            else:
                plot_mesh = pyvista.PolyData(trajectories[step])
                         
        if timing is not None:
            text_actor = plotter.add_text(f"{timing[step]:>2.2f} ms", **text_kwargs)
        if scalars is not None:
            point_actor = plotter.add_mesh(plot_mesh, scalars=scalars[step], **mesh_kwargs)
        else:
            point_actor = plotter.add_mesh(plot_mesh, **mesh_kwargs)

        if vectors is not None:
            if multi_vecs:
                arrow_actors = [plotter.add_arrows(cent=vector_pos[step], direction=vectors[i][step],
                                                   **vector_kwargs[i]) for i in range(len(vectors))]
            else:
                arrow_actor = plotter.add_arrows(cent=vector_pos[step],
                                                 direction=vectors[step],
                                                 **vector_kwargs)

        if visualize_trajectories:
            if step >= 1:
                plot_mesh["disp"] = trajectories[step]-trajectories[step-1]
                glyphs = plot_mesh.glyph(orient="disp", geom=pyvista.Line())
                glyph_actors = plotter.add_mesh(glyphs, **trajectory_kwargs)
                trace_glyphs.append(glyph_actors)
                if len(trace_glyphs) > visualize_trajectories_max_length:
                    glyph_to_remove = trace_glyphs.pop(0)
                    plotter.remove_actor(glyph_to_remove)

                    
        if projection_curve is not None:
            if step >= 1:
                pos_a = np.repeat([projection_curve[step-1]], 3, axis=0)
                pos_b = np.repeat([projection_curve[step]], 3, axis=0)
                pos_a[0, 2], pos_a[1, 0], pos_a[2, 1] = 0, 0, 0
                pos_b[0, 2], pos_b[1, 0], pos_b[2, 1] = 0, 0, 0
                proj_glyphs = [pyvista.Line(a, b) for a, b in zip(pos_a, pos_b)]
                glyph_actors = [plotter.add_mesh(pg, show_scalar_bar=False, color=c, **projection_kwargs) 
                                for pg, c in zip(proj_glyphs, ["blue", "green", "red"])]
                projection_glyphs.append(glyph_actors)
                if len(projection_glyphs) > max_projection_curve_len:
                    glyph_to_remove = projection_glyphs.pop(0)
                    plotter.remove_actor(glyph_to_remove)
                         
        plotter.render()
        plotter.write_frame()
        if timing is not None:
            plotter.remove_actor(text_actor)
        if vectors is not None:
            if multi_vecs:
                for act in arrow_actors:
                    plotter.remove_actor(act)
            else:
                plotter.remove_actor(arrow_actor)

        plotter.remove_actor(point_actor)    
    
    
def get_custom_theme(transparent_background=True):
    custom_theme = pyvista.themes.DefaultTheme()
    custom_theme.background = 'white'
    custom_theme.font.family = 'times'
    custom_theme.font.color = 'black'
    custom_theme.outline_color = "white"
    custom_theme.edge_color = 'grey'
    custom_theme.transparent_background = transparent_background
    return custom_theme


def map_susceptibility_to_xcat_phantom(mesh):
    """ Assigns susceptibility values according to mrxcat labels to a mesh.
    """
    mrxcat_labels = dict(air=0, myoLV=1, myoRV=2, myoLA=3, myoRA=4, bldplLV=5, bldplRV=6, ldplLA=7, bldplRA=8,
                     body=9, muscle=10, brain=11, rbreast=12, lbreast=12, liver=13, gall_bladder=14,
                     right_lung=15, left_lung=16, esophagus=17, esophagus_contents=18, 
                     laryngopharynx=19, st_wall=20, st_cnts=21, pancreas=22, right_kidney_cortex=23,
                     right_kidney_medulla=24, left_kidney_cortex=25, left_kidney_medulla=26,
                     adrenal=27, right_renal_pelvis=28, left_renal_pelvis=29, spleen=30,
                     rib=31, cortical_bone=32, spine=33, bone_marrow=35, art=36, vein=37,
                     bladder=38, prostate=39, asc_large_intest=40, trans_large_intest=41, desc_large_intest=42,
                     small_intest=43, rectum=44, sem=45, vas_def=46, test=47, epididymus=48,
                     ejac_duct=49, pericardium=50, cartilage=51, intest_air=52, ureter=53, 
                     urethra=54, lymph=55, lymph_abnormal=56, trach_bronch=57, airway=58, uterus=59, vagina=60,
                     right_ovary=61, left_ovary=62, fallopian_tubes=63, parathyroid=64, thymus=66, 
                     salivary=67, pituitary=68, eye=69, lens=70)
    suszeptibility_map = dict({0: 2.9e-7, 1: -6e-7, 2: -6e-7, 3: -6e-7, 4: -6e-7, 5: -4e-7, 6: -7e-7,
                               7: -4e-7, 8: -7e-7, 9: -5e-7, 13: -4e-7, 15: 0., 16: 0, 36: -4e-7, 37: -7e-7})
    
    sus_field = np.empty(mesh["labels"].shape, dtype=np.float32)
    for lbl in np.unique(mesh["labels"]).astype(int):
        idx = np.where(mesh["labels"] == lbl)
        sus_field[idx] = suszeptibility_map.get(lbl, -5e-7)

    mesh["susceptibility"] = sus_field
    return mesh