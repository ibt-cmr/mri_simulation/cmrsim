"""The submodules contained in this package are a collection of functions and calculations that
are commonly used to prepare the simulation data or analyse and inspect the results"""

__all__ = ["coordinates", "snr", "particle_properties", "display"]

import cmrsim.utils.coordinates
import cmrsim.utils.snr
import cmrsim.utils.particle_properties
import cmrsim.utils.display
import cmrsim.utils.eddy_current

