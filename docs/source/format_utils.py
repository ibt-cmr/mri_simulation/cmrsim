import cmrsim
import os
cur_dir = os.path.dirname(os.path.abspath(__file__))

def format_utility():
    modules = cmrsim.utils.__all__

    static_header = f"Utility Functions\n{'=' * 50}\n\n"""

    body = ""
    for module in modules:
        body += f"\n\n{module}\n{'^' * 50}\n"
        body += "\n".join([f".. autofunction::\t{f'cmrsim.utils.{module}.{name}'}"
                           for name in getattr(cmrsim.utils, module).__all__])

    with open(f"{cur_dir}/api/cmrsim.utils.rst", "w+") as filep:
        filep.write(static_header + body)


if __name__ == "__main__":
    format_utility()
