Getting Started
=======================

.. toctree::
    :hidden:
    :caption: Set up

    why
    authors
    installation

.. toctree::
    :hidden:
    :caption: Getting Started

    input_definition
    starting_analytic
    starting_bloch



Set up
-------------

.. grid:: 2

    .. grid-item-card::  Why CMRsim?
        :link: why
        :link-type: doc

        Learn more about the idea behind CMRsim.

    .. grid-item-card::  Authors & Citation
        :link: authors
        :link-type: doc

        Using CMRsim in your research? Please consider citing or
        acknowledging us.

.. grid:: 1

    .. grid-item-card::  Installation
        :link: installation
        :link-type: doc

        CMRsim offers a pip-installable python package as well as Docker-containers.
        Find out what suits your needs on the installation page


Start working with CMRsim
-------------------------------------

.. grid:: 1

    .. grid-item-card::  Simulation Input
        :link: input_definition
        :link-type: doc

        See how to pass your digital phantoms to cmrsim simulations.

.. grid:: 2

    .. grid-item-card:: Analytic simulations
        :link: starting_analytic
        :link-type: doc

        Are you interested in generating images with varying contrast and diverse fourier
        encoding using the corresponding analytic signal equations?
        Then checkout the introduction to analytic simulations!

    .. grid-item-card:: Bloch simulations
        :link: starting_bloch
        :link-type: doc

        Running full bloch simulations of static or moving particles including magnetization
        history is what you are looking for?
        Then checkout the introduction to bloch simulations!

