Why CMRsim?
============

1. Flexible Integration of Motion in MR-simulations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The cmrsim framework implements a concisely defined interface to include motion into simulations.
For Bloch-simulations, an independent trajectory module that implements the function to increment
the particle position for a certain time step is everything you need. Implementations for e.g.
Flow and cardiac mesh contractions are provided. Extending these functionalities can be achieved
independently from other simulation components. The default mode for analytic simulations is
to precompute the motion before evaluating the signal module. If the number of material points is
too large for complete pre-computation the motion evaluation can easily be included into the data
stream, by using the tf.data.Dataset API.

2. Modern Tech-Stack
^^^^^^^^^^^^^^^^^^^^^^^
GPU - acceleration, driver compatibility, graphical user-interface and 3D-mesh handling is not
managed by the cmrsim package, but it uses existing solutions like TensorFlow2,
Jupyter-Lab + Widgets and pyvista. This way, the maintenance overhead is reduced while also
allowing for presenting fixed experiments with interactive user input. Furthermore, all software
dependencies are open source and therefore perfectly suited for the research context.

3. Native Integration of Machine-Learning
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
All computationally demanding operation are written, using the Tensorflow python API. Furthermore,
the overall software architecture is truly modular. This enables replacing certain computations,
like motion with network-based approximations for potential speed-up. Also, using the simulation
output to test deep-learning algorithms for reconstruction or even using the simulator as
training environment in re-enforcement learning setups is less complicated.

3. Documentation, Unit-testing & Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
CMRsim offers a extensive documentation to the presented functionality and a structured display of
the underlying assumption. Also, the project contains unit-tests to ensure the correctness for
the stated assumption and reduce the risk of unintentionally breaking compatibility on extending
the package. The tests are run before every merge into the main version including a coverage report
and a proper versioning is established. Finally, the written code aims at following the PEP styling
guide-lines, which is evaluated by PyLint.