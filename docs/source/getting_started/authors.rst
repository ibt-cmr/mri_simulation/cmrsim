Authors & Citation
====================

CMRseq is mainly developed and maintained by Jonathan Weine and Charles McGrath
at the Cardiovascular Magnetic Resonance Imaging group at `ETH Zurich`_.

.. _ETH Zurich: https://cmr.ethz.ch/

Contact: weine@biomed.ee.ethz.ch

At this moment there is no citable publication available for this package.
If you find cmrsim useful for your work consider referencing this web-page. Hopefully, in the
future this page will contain the corresponding citation instructions.