Installation
==============


Pip
^^^^
CMRsim can be installed using the python package manager :code:`pip`. The package can be found
in the `registry`_  of the repository hosted on the ETH-Gitlab instance

.. _registry: https://gitlab.ethz.ch/jweine/cmrsim/-/packages

**Versioning:**
Stable release versions are denoted with the ususal :code:`major.minor` notation. Additionally,
development versions are constantly updated on solved issued and can be installed using the
:code:`major.minor.devX` notation

Installation in command line:

.. code-block::

    pip install cmrsim --extra-index-url https://__token__:<your_personal_token>@gitlab.ethz.ch/api/v4/projects/23798/packages/pypi/simple


To install cuda and cdnn for GPU accellaration (requires nvidia drivers to be installed on your system), from version 0.31 you can use:

.. code-block::
    
    pip install cmrsim[cuda]


Requirements
^^^^^^^^^^^^^^^^
CMRsim does rely on functionality of 3 non-standard python stack packages:

1. `TensorFlow`_: Pretty much all computationally heavy calculations.
2. `Pyvista`_: For processing and visualizing graphs (e.g. 3D Velocity fields).
3. `CMRseq`_: For defining MR-sequences used for Fourier-Encoding calculation and Bloch-simulation.

.. _TensorFlow: https://www.tensorflow.org/
.. _Pyvista: https://docs.pyvista.org/
.. _CMRseq: https://people.ee.ethz.ch/~jweine/cmrseq/index.html

Further (already indirectly included) requirements are:
-h5py~=2.10.0
-scipy>=1.4.1
-tqdm


Docker
^^^^^^^^
For each major release a set of docker images are compiled and published in the
`container registry`_ associated with repository. Within the docker images, all requirements
are installed for simulation deployment.

.. _container registry: https://gitlab.ethz.ch/jweine/cmrsim/container_registry

Furthermore, there are three image-tags available. To use the pull the docker image
use (e.g. for jupyter tag):

.. code-block::

    docker pull registry.ethz.ch/jweine/cmrsim/jupyter:latest

CPU
""""
Based on the Tensorflow:...-cpu:latest docker image. Containing all cmrsim dependencies and cmrsim

GPU
""""
Based on the Tensorflow:...-gpu:latest docker image. Containing all cmrsim dependencies and cmrsim

Jupyter
""""""""
Based on the gpu:latest tag, but also includes an installation of jupyter-lab for interactive
simulation / rendering.


