Digital Phantoms
===================================

In CMRsim, digital objects are defined as a set of particles bearing physical properties, without
assuming a spatial structure on simulation time. The input format for all simuations
within CMRsim are python dictionaries, where the keys denote the physical properties and the values
of the dictionary are :code:`np.ndarrays` that have at least 1 axis. Mandatory axis has the
dimensionality of the number of all particles in the object.

Generally, the required entries in the dictionary are defined by the composed simulator (as you
will see in the subsequent tutorial sections). However, there are mandatory inputs with
specific shapes for differing for analytic and bloch simulations as shown below:

.. card:: Input to Bloch simulation

    .. code-block::

        digital_phantom = {
            "M0" : np.ndarray[dtype = np.complex64, shape = (#points, )]             # rho
            "magnetization" : np.ndarray[dtype = np.complex64, shape = (#points, )]  # (M+, M-, Mz)
            "T1" : np.ndarray[dtype = np.float32, shape = (#points, )],
            "T2" : np.ndarray[dtype = np.float32, shape = (#points, )]
            "initial_positions" : np.ndarray[dtype = np.float32, shape = (#points, 3)]
        }

For analytic simulations the inputs have additional axes denoted with :code:`#repetitions` and
:code:`#k-space-samples`. The reason for these axes to be present, lies within an efficient broad
casting logic allowing an efficient evaluation of multiple simulation configurations. To learn
more about that checkout the tutorial page on analytic simulations.

.. card:: Input to Analytic simulation

    .. code-block::

        digital_phantom = {
            "M0" : np.ndarray[dtype = np.complex64, shape = (#points, #repetitions, #k-space-samples)],
            "r_vectors": np.ndarray[dtype = np.float32, shape = (#points, #repetitions, #k-space-samples, 3(XYZ))],
            "T1" : np.ndarray[dtype = np.float32, shape = (#points, #repetitions, #k-space-samples)],
            "T2" : np.ndarray[dtype = np.float32, shape = (#points, #repetitions, #k-space-samples)]
        }

Quantities, that spatially vary (e.g. coil sensitivity or velocity within a velocity field)
are looked up on simulation time. Physical properties that depend on the spatial structure of the
digital object need to be computed outside of the simulation call and assigned to the
particles on demand.
