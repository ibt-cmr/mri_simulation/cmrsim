import cmrsim
import glob
import os

cur_dir = os.path.dirname(os.path.abspath(__file__))

def format_dataset_index():
    all_modules = glob.glob(f"{cur_dir}/api/datasets/cmrsim.datasets.*.rst")
    all_modules = [s.replace(f"{cur_dir}/api/", "") for s in all_modules]

    header = f"Dataset Modules\n{'=' * 50}\n\n"
    toctree = f".. toctree::\n   :hidden:\n\n   " + "\n   ".join(all_modules)
    paragraph_heading = f"{cmrsim.datasets.__doc__}\n\nSummary\n{'-' * 50}\n\n" \
                        f"Currently available dataset modules:\n\n"
    summary_entries = [f".. autosummary:: \tcmrsim.datasets.{name}"
                       for name in cmrsim.datasets.__all__]

    text = header + toctree + "\n\n" + paragraph_heading[1:] + "\n".join(summary_entries) + "\n"

    with open(f"{cur_dir}/api/cmrsim.datasets.rst", "w+") as filep:
        filep.write(text)


def format_dataset_classes():
    for name in cmrsim.datasets.__all__:
        text = f"{name}\n{'=' * 50}\n\n.. autoclass::\t{f'cmrsim.datasets.{name}'}"
        with open(f"{cur_dir}/api/datasets/cmrsim.datasets.{name}.rst", "w+") as filep:
            filep.write(text)


if __name__ == "__main__":
    os.makedirs(f"{cur_dir}/api/datasets", exist_ok=True)
    format_dataset_classes()
    format_dataset_index()
