.. cmrsim documentation master file, created by
   sphinx-quickstart on Thu Jul 30 15:43:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. carousel::
   :show_controls:
   :show_indicators:

   .. image:: _static/banner1.svg
   .. image:: https://cdn.pixabay.com/photo/2017/04/11/15/55/animals-2222007_960_720.jpg



------------------------------------------------------------------------------------------------



Welcome to CMRsim!
==================================

.. image:: _static/Logo_cmrsim_moving_light.svg
   :class: only-light
   :align: right
   :scale: 500 %

.. image:: _static/Logo_cmrsim_moving_dark.svg
   :class: only-dark
   :align: right
   :scale: 500 %


CMRsim is a MRI - simulation framework that features bloch-simulations as well as analytic signal
evaluations for **moving objects** with an easy to use API written in pure python.
The computationally demanding calculations are implemented in `TensorFlow2`_ and therefore are
seamlessly executable on CPU and GPU environments, while also making efficient use of computational
resources!

.. _TensorFlow2: https://www.tensorflow.org/

What makes CMRsim special?

- CMRsim features bloch-simulations as well as simulations using analytical signal equations and even allows to combine elements of both.
- Convenient support for **moving** particles!
- Modularity that makes to designing comparative simulation experiments convenient.
- Architecture and implementation that allows to easily include your own simulation modules.
- Reproducibility by using versioned and documented open-source software.
- Easily scalable to detailed digital phantoms

.. toctree::
   :maxdepth: 3
   :hidden:

   getting_started/index
   examples
   api/index


Indices and tables
--------------------

* :ref:`genindex`
* :ref:`search`

