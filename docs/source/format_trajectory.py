import cmrsim
import glob
import os

cur_dir = os.path.dirname(os.path.abspath(__file__))

def format_trajectory_index():
    all_modules = glob.glob(f"{cur_dir}/api/trajectory/cmrsim.trajectory.*.rst")
    all_modules = [s.replace(f"{cur_dir}/api/", "") for s in all_modules]
    header = f"Trajectory Modules\n{'=' * 50}\n\n"
    toctree = f".. toctree::\n   :hidden:\n\n   " + "\n   ".join(all_modules)
    paragraph_heading = f"{cmrsim.trajectory.__doc__}\n\nSummary\n{'-' * 50}\n\n" \
                        f"Currently available trajectory modules:\n\n"
    summary_entries = [f".. autosummary:: \tcmrsim.trajectory.{name}"
                       for name in cmrsim.trajectory.__all__]

    # For a non-identified reason paragraph_heading string starts with a leading white-space
    text = header + toctree + "\n\n" + paragraph_heading[1:] + "\n".join(summary_entries) + "\n"

    with open(f"{cur_dir}/api/cmrsim.trajectory.rst", "w+") as filep:
        filep.write(text)


def format_trajectory_classes():
    for name in cmrsim.trajectory.__all__:
        text = f"{name}\n{'=' * 50}\n\n.. autoclass::\t{f'cmrsim.trajectory.{name}'}"
        with open(f"{cur_dir}/api/trajectory/cmrsim.trajectory.{name}.rst", "w+") as filep:
            filep.write(text)


if __name__ == "__main__":
    os.makedirs(f"{cur_dir}/api/trajectory", exist_ok=True)
    format_trajectory_classes()
    format_trajectory_index()
