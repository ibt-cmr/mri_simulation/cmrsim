import cmrsim
import os

cur_dir = os.path.dirname(os.path.abspath(__file__))

def _format_table_row(lcol_string, rcol_string):
    s = f"\n|{lcol_string :<40}|{rcol_string:<100}|"
    s += f"\n+{'-' * 40}+{'-' * 100}+"
    return s


def _list_required_properties():
    readme_string = f"+{'-' * 40}+{'-' * 100}+ " + \
                 f"\n|{'Sub - module':<40}|{'Required Quantities':<100}|" + \
                 f"\n+{'=' * 40}+{'=' * 100}+"

    for class_name in cmrsim.analytic.contrast.__all__:
        obj = getattr(cmrsim.analytic.contrast, class_name)
        tuple_str = str(obj.__dict__.get('required_quantities'))
        readme_string += _format_table_row(class_name, tuple_str)

    readme_string += "\n"
    return readme_string


def format_contrast():

    static_header = f"Contrast Modules\n{'=' * 50}\n\n"\
        "All contrast modules are implemented as a subclass to `BaseSignalModel`.\n"\
        "To ensure extensibility stable functionality all modules need to take the same input"\
        "arguments and return a tensor of the same shape as the input tensor. \n"\
        "To make the required properties provided as input availabe to the \_\_call\_\_ method"\
        "during runtime (while maintaining the uniform calling-argument signature) they have to be"\
        "passed via \*\*kwargs (keyword-arguments).\n"\
        "To make sure they are loaded during runtime, all required properties have to be specified"\
        "in the 'required\_quantities' class attribute. A table of the implemented modules along"\
        "with their required input is shown below:\n"

    # Insert table of required Quantities
    required_quanitites = _list_required_properties()
    body = static_header + f"\nRequired Quantities \n{'-' * 50}\n\n" + required_quanitites + "\n\n"

    # Create summary table
    summary_strings = [f".. autosummary:: \tcmrsim.analytic.contrast.{name}" for name in
                       cmrsim.analytic.contrast.__all__]
    body += f"\nSummary\n{'-' * 50}\n\n" + "\n".join(summary_strings) + "\n\n"

    # Create actual module references
    body += f"\nModules\n{'-' * 50}\n\n"
    body += "\n".join([f".. autoclass::\t{f'cmrsim.analytic.contrast.{name}'}"
                       for name in cmrsim.analytic.contrast.__all__])

    with open(f"{cur_dir}/api/analytic/contrast.rst", "w+") as filep:
        filep.write(body)


def format_encoding():
    static_header = f"""Encoding Modules\n{'=' * 50}\n\n"""

    # Create summary table
    summary_strings = [f".. autosummary:: \tcmrsim.analytic.encoding.{name}" for name in
                       cmrsim.analytic.encoding.__all__]
    body = static_header + f"\nSummary\n{'-' * 50}\n\n" + "\n".join(summary_strings) + "\n\n"

    # Create actual module references
    body += f"\nModules\n{'-' * 50}\n\n"
    body += "\n".join([f".. autoclass::\t{f'cmrsim.analytic.encoding.{name}'}"
                       for name in cmrsim.analytic.encoding.__all__])

    with open(f"{cur_dir}/api/analytic/encoding.rst", "w+") as filep:
        filep.write(body)


def format_classes():

    s = f"AnalyticSimulation\n{'=' * 50}\n\n"
    s += ".. autoclass:: cmrsim.analytic.AnalyticSimulation"
    with open(f"{cur_dir}/api/analytic/AnalyticSimulation.rst", "w+") as filep:
        filep.write(s)

    s = f"CompositeSignalModel\n{'=' * 50}\n\n"
    s += ".. autoclass:: cmrsim.analytic.CompositeSignalModel"
    with open(f"{cur_dir}/api/analytic/CompositeSignalModel.rst", "w+") as filep:
        filep.write(s)


if __name__ == "__main__":
    os.makedirs(f"{cur_dir}/api/analytic", exist_ok=True)
    format_contrast()
    format_encoding()
    format_classes()
