API reference
==============


.. toctree::
   :maxdepth: 2
   :hidden:

   cmrsim.trajectory
   cmrsim.bloch
   cmrsim.analytic
   cmrsim.datasets
   cmrsim.simulation_templates
   cmrsim.utils


.. card:: Overview of component-functionality:

    .. image:: ../_static/api/overview_simulation.svg

.. card:: Overview of the CMRSIM package-components

    .. image:: ../_static/api/api_overview.png

