Analytic Simulations
========================

.. toctree::
    :maxdepth: 2
    :hidden:

    analytic/AnalyticSimulation
    analytic/CompositeSignalModel
    analytic/contrast
    analytic/encoding

.. card:: Flowchart of analytic simulation internal

    .. image:: ../_static/api/flow_analytic.png
