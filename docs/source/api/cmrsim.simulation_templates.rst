Simulation Templates
==========================

Summary
--------------------------------------------------

.. autosummary:: 	cmrsim.simulation_templates.FlowSimulation
.. autosummary:: 	cmrsim.simulation_templates.FlowSimulationMultiVenc


FlowSimulation
--------------------------------------------------
.. autoclass::	cmrsim.simulation_templates.FlowSimulation

FlowSimulationMultiVenc
--------------------------------------------------
.. autoclass::	cmrsim.simulation_templates.FlowSimulationMultiVenc