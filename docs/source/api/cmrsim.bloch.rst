Bloch Simulations
======================

.. toctree::
    :maxdepth: 2
    :hidden:

    bloch/GeneralBlochOperator
    bloch/ParallelReceiveBlochOperator
    bloch/ideal_operators
    bloch/submodules

.. card:: Flowchart of Bloch simulation internals

    .. image:: ../_static/api/flow_bloch.png