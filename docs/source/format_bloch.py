import cmrsim
import os

cur_dir = os.path.dirname(os.path.abspath(__file__))

def format_submodules():
    static_header = f"Submodules \n{'=' * 50}\n\nSummary\n{'-' * 50}\n\n"
    summary_strings = [f".. autosummary:: \tcmrsim.bloch.submodules.{name}" for name in
                       cmrsim.bloch.submodules.__all__]

    body = static_header + "\n".join(summary_strings) + f"\n\nModules\n{'-' * 50}\n"
    body += "\n".join([f".. autoclass::\t{f'cmrsim.bloch.submodules.{name}'}"
                       for name in cmrsim.bloch.submodules.__all__])

    with open(f"{cur_dir}/api/bloch/submodules.rst", "w+") as filep:
        filep.write(body)


def format_ideal():

    header = f"Ideal Operators\n{'=' * 50}\n\n" + f"\nSummary\n{'-' * 50}\n\n"
    temp = [f".. autosummary:: \tcmrsim.bloch._ideal.{name}" for name in
            cmrsim.bloch._ideal.__all__]

    body = header + "\n".join(temp) + "\n\n"
    body += f"\nModules\n{'-' * 50}\n"
    body += "\n".join([f"\n{name}\n{'.' * 50}\n.. autoclass::\t{f'cmrsim.bloch._ideal.{name}'}"
                       for name in cmrsim.bloch._ideal.__all__])

    with open(f"{cur_dir}/api/bloch/ideal_operators.rst", "w+") as filep:
        content = filep.readlines()
        content.insert(3, body)
        filep.seek(0)
        content = "".join(content)
        filep.write(content)


def format_classes():

    s = f"GeneralBlochOperator\n{'=' * 50}\n\n"
    s += ".. autoclass:: cmrsim.bloch._generic.GeneralBlochOperator"
    with open(f"{cur_dir}/api/bloch/GeneralBlochOperator.rst", "w+") as filep:
        filep.write(s)

    s = f"ParallelReceiveBlochOperator\n{'=' * 50}\n\n"
    s += ".. autoclass:: cmrsim.bloch._multi_coil.ParallelReceiveBlochOperator"
    with open(f"{cur_dir}/api/bloch/ParallelReceiveBlochOperator.rst", "w+") as filep:
        filep.write(s)


if __name__ == "__main__":
    os.makedirs(f"{cur_dir}/api/bloch", exist_ok=True)
    format_classes()
    format_ideal()
    format_submodules()
