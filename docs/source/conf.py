# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import sys
from typing import Any
from bs4 import BeautifulSoup

try:
    import cmrsim
except ImportError:
    module_path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    print(module_path), os.listdir(module_path)
    sys.path.insert(0, os.path.abspath(module_path))
    _ = list(os.walk(os.path.dirname(os.path.dirname(os.path.dirname(__file__))) + '/cmrsim'))
    submodule_paths = [_[0][0] + f'/{s}' for s in _[0][1]]
    for p in submodule_paths:
        sys.path.append(p)
    import cmrsim


# -- Project information -----------------------------------------------------
project = 'cmrsim'
copyright = '2022, Jonathan Weine & Charles McGrath'
author = 'Jonathan Weine & Charles McGrath'

# Define the version we use for matching in the version switcher.
version_switcher_url = "https://people.ee.ethz.ch/~jweine/cmrsim/latest/_static/switcher.json"
version_match = os.environ.get("CMRSIM_BUILD_VERSION")
if not version_match:
    release = cmrsim.__version__
    if "dev" in release:
        version_match = "dev"
    else:
        version_match = "v" + release
# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
import autodocsumm

extensions = ['sphinx.ext.todo',
              'sphinx.ext.viewcode',
              'sphinx.ext.autodoc',
              'sphinx.ext.autosummary',
              'sphinx.ext.mathjax',
              'autodocsumm',
              'sphinx_autodoc_typehints',
              'IPython.sphinxext.ipython_console_highlighting',
              'sphinx_carousel.carousel',
              'sphinx_design',
              'nbsphinx',
              'sphinx_gallery.load_style',  # load CSS for gallery (needs SG >= 0.6)
              'sphinx_codeautolink',  # automatic links from code to documentation
              'sphinx.ext.intersphinx',  # links to other Sphinx projects (e.g. NumPy)
              ]

autodoc_typehints = 'both'
autodoc_typehints_format = 'short'
nbsphinx_allow_errors = True

intersphinx_mapping = {
    'IPython': ('https://ipython.readthedocs.io/en/stable/', None),
    'matplotlib': ('https://matplotlib.org/', None),
    'numpy': ('https://docs.scipy.org/doc/numpy/', None),
    'python': ('https://docs.python.org/3/', None),
}


mathjax3_config = {
    'tex': {'tags': 'ams', 'useLabelIds': True},
}

# Support for notebook formats other than .ipynb
nbsphinx_custom_formats = {
    '.pct.py': ['jupytext.reads', {'fmt': 'py:percent'}],
    '.md': ['jupytext.reads', {'fmt': 'Rmd'}],
}

import matplotlib.pyplot

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []
add_module_names = False

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'pydata_sphinx_theme'
add_module_name = False
html_theme_options = {
    "show_prev_next": False,
    "show_nav_level": 2,
    "icon_links": [
        {
            "name": "GitLab",
            "url": "https://gitlab.ethz.ch/ibt-cmr/mri_simulation/cmrsim/",
            "icon": "_static/gitlab-logo-100.svg",
            "type": "local",
        }
    ],
    "logo": {
        "image_light": "Logo_cmrsim.svg",
        "image_dark": "Logo_cmrsim_dark.svg",
        "text": "CMRsim",
        "scale": "160%"
    },
    "switcher": {
        "json_url": version_switcher_url,
        "version_match": version_match
    },
    "navbar_start": ["navbar-logo", "version-switcher"],
    "secondary_sidebar_items": ["class_page_toc"],
    "page_sidebar_items": ["page-toc"],
    "navigation_with_keys": True
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_templates_path = ['_templates']
html_css_files = ['css/custom.css', ]
html_logo = "_static/Logo_cmrsim.svg"
html_sidebars = {"index": []}

autodoc_default_options = {
    'autosummary': True,
    'special-members': '__call__',
    'members': True,
    'attributes': True,
    'properties': True,
    'show_inheritance': True,
    'inherited-members': False,
    'member-order': "groupwise",
    'imported-members': False,
    'autoclass_content': 'both',
    'autodoc_class_signature': 'mixed'
}

nbsphinx_thumbnails = {
    # Default
    'example_gallery/analytic_simulation/*': '_static/Logo_cmrsim.png',
    'example_gallery/bloch_simulation/*': '_static/Logo_cmrsim.png',
    'example_gallery/trajectory_modules/*': '_static/Logo_cmrsim.png',
    'example_gallery/datasets/*': '_static/Logo_cmrsim.png',
    # Datasets
    'example_gallery/datasets/cardiac_mesh_dataset': '_static/notebook_thumbnails/dataset_mesh.gif',
    'example_gallery/datasets/refilling_flow_dataset': '_static/notebook_thumbnails/explore_dataset.svg',
    'example_gallery/datasets/regular_grid_dataset': '_static/notebook_thumbnails/regular_grid_dataset.png',
    # Trajectory modules
    'example_gallery/trajectory_modules/flow_trajectory_module': '_static/notebook_thumbnails/trajectory_laminar_flow.png',
    'example_gallery/trajectory_modules/turbulent_flow': '_static/notebook_thumbnails/trajectory_turbulent_flow.png',
    'example_gallery/trajectory_modules/taylor_trajectories': '_static/notebook_thumbnails/taylor_trajectories.gif',
    'example_gallery/trajectory_modules/breathing_motion_module': '_static/notebook_thumbnails/breathing_motion_module.gif',
    'example_gallery/trajectory_modules/cardiac_mesh_pod': '_static/notebook_thumbnails/pod_trajectory.png',
    'example_gallery/trajectory_modules/contracting_cardiac_mesh_with_random_disp': '_static/notebook_thumbnails/trajectory_bulk_and_diffusion.gif',
    # Analytic
    'example_gallery/analytic_simulation/1_basic_functionality': '_static/notebook_thumbnails/analytic_base.png',
    'example_gallery/analytic_simulation/2_motion': '_static/notebook_thumbnails/analytic_motion.gif',
    'example_gallery/analytic_simulation/3_multiple_coils': '_static/notebook_thumbnails/analytic_multiple_coils.png',
    'example_gallery/analytic_simulation/BSSFP_offresonance': '_static/notebook_thumbnails/analytic_static_bssfp.png',
    # 'example_gallery/analytic_simulation/cardiac_dti_with_background': '_static/notebook_thumbnails/analytic_cdti_stylized.png',
    'example_gallery/analytic_simulation/slice_profile_weighting': '_static/notebook_thumbnails/analytic_slice_select.png',
    # Bloch
    'example_gallery/bloch_simulation/static_cartesian_SEEPI_shepp': '_static/notebook_thumbnails/static_bloch_epi.svg',
    'example_gallery/bloch_simulation/static_cartesian_sGRE_shepp': '_static/notebook_thumbnails/static_bloch_spoiled_gre.svg',
    'example_gallery/bloch_simulation/static_BSSFP_offresonance': '_static/notebook_thumbnails/bloch_static_bssfp_offres.png',
    'example_gallery/bloch_simulation/static_CPMG_t2star': '_static/notebook_thumbnails/t2s_cpmg.svg',
    'example_gallery/bloch_simulation/static_multi_coil_bssfp': '_static/notebook_thumbnails/bloch_static_multicoil_bssfp.png',
    'example_gallery/bloch_simulation/flow_venc_sGRE_turbUBend': '_static/notebook_thumbnails/bloch_flow_venc_sGRE_turbUBend.png',
    'example_gallery/bloch_simulation/flow_venc_sGRE_stenosis_sequential': '_static/notebook_thumbnails/flow_bloch_sequential.svg',
    'example_gallery/bloch_simulation/movmesh_bssfp': '_static/notebook_thumbnails/bloch_movemesh_bssfp.png',
    'example_gallery/bloch_simulation/static_GIRF_GRE': '_static/notebook_thumbnails/static_GIRF_GRE.png',
}

def _setup_navbar_side_toctree(app: Any):


    def add_class_toctree_function(app: Any, pagename: Any, templatename: Any,
                                   context: Any, doctree: Any):
        def get_class_toc() -> Any:
            try:
                soup = BeautifulSoup(context["body"], "html.parser")

                matches = soup.find_all('dl')
                if matches is None or len(matches) == 0 or isinstance(matches[0], str):
                    return ""
                items = []
                deeper_depth = matches[0].find('dt').get('id').count(".")
                for match in matches:
                    match_dt = match.find('dt')
                    if match_dt is not None and match_dt.get('id') is not None:
                        current_depth = match_dt.get('id').count(".")
                        current_link = match.find(class_="headerlink")
                        current_title = match_dt.get('id').split(".")[-1]
                        current_title = current_title

                        if current_link is not None:
                            if deeper_depth > current_depth:
                                deeper_depth = current_depth
                            if deeper_depth == current_depth:
                                items.append({
                                    "title": current_title,
                                    "link": current_link["href"],
                                    "attributes_and_methods": []
                                })
                            if deeper_depth < current_depth:
                                items[-1]["attributes_and_methods"].append(
                                    {
                                        "title": "--" + current_title,
                                        "link": current_link["href"],
                                    }
                                )
                return items
            except:
                return ""

        context["get_class_toc"] = get_class_toc

    app.connect("html-page-context", add_class_toctree_function)

def setup(app: Any):
    for setup_function in [
        _setup_navbar_side_toctree,
    ]:
        setup_function(app)
