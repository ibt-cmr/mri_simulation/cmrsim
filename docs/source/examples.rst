Examples
==========

.. card::

    .. nbgallery::
        :caption: Datasets

        example_gallery/datasets/refilling_flow_dataset
        example_gallery/datasets/cardiac_mesh_dataset
        example_gallery/datasets/regular_grid_dataset


.. card::

    .. nbgallery::
        :caption: Trajectory Modules

        example_gallery/trajectory_modules/flow_trajectory_module
        example_gallery/trajectory_modules/turbulent_flow
        example_gallery/trajectory_modules/taylor_trajectories
        example_gallery/trajectory_modules/cardiac_mesh_pod
        example_gallery/trajectory_modules/breathing_motion_module
        example_gallery/trajectory_modules/contracting_cardiac_mesh_with_random_disp

.. card::

    .. nbgallery::
        :caption: Bloch Simulations - Static Phantoms

        example_gallery/bloch_simulation/static_cartesian_SEEPI_shepp
        example_gallery/bloch_simulation/static_cartesian_sGRE_shepp
        example_gallery/bloch_simulation/static_CPMG_t2star
        example_gallery/bloch_simulation/static_BSSFP_offresonance
        example_gallery/bloch_simulation/static_multi_coil_bssfp
        example_gallery/bloch_simulation/static_GIRF_GRE

    .. nbgallery::
        :caption: Bloch Simulations - Flowing Particles

        example_gallery/bloch_simulation/flow_venc_sGRE_turbUBend
        example_gallery/bloch_simulation/flow_venc_sGRE_stenosis_sequential
        example_gallery/bloch_simulation/flow_mvenc_sGRE_ubend_parallel
        example_gallery/bloch_simulation/flow_bssfp_submodules_aorta

    .. nbgallery::
        :caption: Bloch Simulations - Contracting Mesh

        example_gallery/bloch_simulation/movmesh_bssfp
        example_gallery/bloch_simulation/movmesh_general_bloch

.. card::

    .. nbgallery::
        :caption: Analytic Simulations

        example_gallery/analytic_simulation/1_basic_functionality
        example_gallery/analytic_simulation/2_motion
        example_gallery/analytic_simulation/cardiac_dti_with_background
        example_gallery/analytic_simulation/3_multiple_coils
        example_gallery/analytic_simulation/BSSFP_offresonance
        example_gallery/analytic_simulation/slice_profile_weighting