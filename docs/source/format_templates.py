import cmrsim
import os

cur_dir = os.path.dirname(os.path.abspath(__file__))

def format_templates():

    header = f"\nSummary\n{'-' * 50}\n\n"
    temp = [f".. autosummary:: \tcmrsim.simulation_templates.{name}" for name in
            cmrsim.simulation_templates.__all__]

    body = header + "\n".join(temp) + "\n\n"
    body += "\n".join([f"\n{name}\n{'-' * 50}\n.. autoclass::\t{f'cmrsim.simulation_templates.{name}'}"
                       for name in cmrsim.simulation_templates.__all__])


    with open(f"{cur_dir}/api/cmrsim.simulation_templates.rst", "r+") as filep:
        content = filep.readlines()
        content.insert(3, body)
        filep.seek(0)
        content = "".join(content)
        filep.write(content)


if __name__ == "__main__":
    format_templates()
