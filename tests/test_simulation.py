import unittest
import os

import tensorflow as tf
tf.config.run_functions_eagerly(True)

import numpy as np

import cmrsim
from cmrsim.analytic.simulation import AnalyticSimulation
from cmrsim.trajectory import StaticParticlesTrajectory
import tests.phantoms

output_dir = os.path.abspath(f"{os.path.dirname(__file__)}/test_output/simulation/")
os.makedirs(output_dir, exist_ok=True)


class DummyAnalyticSimulation(AnalyticSimulation):
    def build(self, **kwargs):  # noqa
        # Encoding definition
        encoding_module = cmrsim.analytic.encoding.EPI(field_of_view=(0.31, 0.112),
                                                       sampling_matrix_size=(121, 43),
                                                       bandwidth_per_pixel=1300., blip_duration=0.,
                                                       k_space_segments=43,
                                                       acquisition_start=-25.7692,
                                                       absolute_noise_std=0.)

        sens_maps = tf.constant(np.load(f'{os.path.dirname(__file__)}/resources/coil_'
                                        f'sensitivities.npy').transpose([1, 2, 3, 0]),
                                dtype=tf.complex64)

        # Signal module construction
        mdims = np.array([(-0.155, 0.155), (-0.055, 0.055), (0., 0.1)], dtype=np.float32)
        coil_sens_module = cmrsim.analytic.contrast.CoilSensitivity(sens_maps, mdims,
                                                                    device="CPU:0")

        # Forward model composition
        forward_model = cmrsim.analytic.CompositeSignalModel(coil_sens_module)

        # Recontruct module
        recon_module = cmrsim.reconstruction.Cartesian2DMultiCoil(
                            sample_matrix_size=(121, 43),
                            coil_channel_axis=0,
                            coil_sensitivities=tf.transpose(sens_maps[:121, :43, 0, :], [2, 1, 0]))
        return forward_model, encoding_module, recon_module


class TestBasic(unittest.TestCase):
    def setUp(self) -> None:
        pdict, pref, coords = tests.phantoms.define_cylinder_phantom((0.2, 0.1), (50, 50))
        self.dataset = tests.phantoms.dict_to_base(pdict)

    def test_builds(self):
        with self.subTest("Catch missing arguments"):
            self.assertRaises(ValueError, lambda: DummyAnalyticSimulation())

        with self.subTest("From Blocks"):
            encoding_module = cmrsim.analytic.encoding.EPI(field_of_view=(0.31, 0.112),
                                                           sampling_matrix_size=(121, 43),
                                                           bandwidth_per_pixel=1300.,
                                                           blip_duration=0.,
                                                           k_space_segments=43,
                                                           acquisition_start=-25.7692,
                                                           absolute_noise_std=0.)
            coils = cmrsim.analytic.contrast.BirdcageCoilSensitivities(
                                (10, 10, 10, 2), np.array(((-1, 1), (-1, 1), (-1, 1))))
            forward_model = cmrsim.analytic.CompositeSignalModel(coils)
            DummyAnalyticSimulation(building_blocks=(forward_model, encoding_module, None))

        with self.subTest("From kwargs"):
            DummyAnalyticSimulation(build_kwargs=dict())

    def test_run(self):
        simulator = DummyAnalyticSimulation(build_kwargs=dict())
        _ = simulator(self.dataset, voxel_batch_size=1000)
    
    def test_map_trajectory(self):
        simulator = DummyAnalyticSimulation(build_kwargs=dict())
        pdict, pref, coords = tests.phantoms.define_cylinder_phantom((0.2, 0.1), (50, 50))
        key1 = [k for k in pdict.keys() if k != "r_vectors"]
        pdict.update({k: pdict[k].reshape(-1, 1, 1) for k in key1})
        pdict["initial_positions"] = pdict.pop("r_vectors").reshape(-1, 3)

        dataset = cmrsim.datasets.AnalyticDataset(pdict)
        tr_mod = StaticParticlesTrajectory()
        tr_signatures = {"r_vectors": np.zeros([1, 1], dtype=np.float32)}
        _ = simulator(dataset, voxel_batch_size=1000, trajectory_module=tr_mod,
                      trajectory_signatures=tr_signatures, additional_kwargs={})
        
    def test_save_load(self):
        simulator = DummyAnalyticSimulation(build_kwargs=dict())
        simulator.save(f"{output_dir}/simulation_checkpoint")
        simulator2 = DummyAnalyticSimulation.from_checkpoint(f"{output_dir}/simulation_checkpoint")

    def test_save_config(self):
        simulator = DummyAnalyticSimulation(build_kwargs=dict())
        res = simulator.configuration_summary

    def test_write_graph(self):
        simulator = DummyAnalyticSimulation(build_kwargs=dict())
        simulator.write_graph(self.dataset, graph_log_dir=f"{output_dir}/graph")
