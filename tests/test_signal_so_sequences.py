import unittest
import os

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from cmrsim.analytic.contrast import SaturationRecoveryGRE, SpinEcho, BSSFP
plt.close("all")


class TestSpinEcho(unittest.TestCase):
    def test_single_te_shape(self):
        """ Test input shapes for a single set of parameters and inputs for a single repetition"""
        m0 = tf.ones(shape=(10, 1, 1), dtype=tf.complex64)
        T1 = tf.ones(shape=(10, 1, 1), dtype=tf.float32)
        T2 = tf.ones(shape=(10, 1, 1), dtype=tf.float32)

        spin_echo_module = SpinEcho(repetition_time=1000., echo_time=50, expand_repetitions=False)
        result = spin_echo_module(m0, T1=T1, T2=T2)
        self.assertTupleEqual(m0.numpy().shape, result.numpy().shape)

        spin_echo_module = SpinEcho(repetition_time=1000., echo_time=50, expand_repetitions=True)
        result = spin_echo_module(m0, T1=T1, T2=T2)
        self.assertTupleEqual(m0.numpy().shape, result.numpy().shape)

    def test_multiple_te_shape(self):
        m0 = tf.ones(shape=(10, 1, 1), dtype=tf.complex64)
        T1 = tf.ones(shape=(10, 1, 1), dtype=tf.float32)
        T2 = tf.ones(shape=(10, 1, 1), dtype=tf.float32)

        spin_echo_module = SpinEcho(repetition_time=tf.constant([1000., 1200], dtype=tf.float32),
                                    echo_time=tf.constant([50., 10.], dtype=tf.float32),
                                    expand_repetitions=True)
        with self.subTest("Single rep input, exp_fac=2"):
            result = spin_echo_module(m0, T1=T1, T2=T2)
            self.assertTupleEqual((10, 2, 1), result.numpy().shape)

        with self.subTest("Single rep input, exp_fac=2, T1-->2"):
            T1 = tf.ones(shape=(10, 2, 1), dtype=tf.float32)
            result = spin_echo_module(m0, T1=T1, T2=T2)
            self.assertTupleEqual((10, 2, 1), result.numpy().shape)

        with self.subTest("2 rep input, exp_fac=2, T1-->2"):
            T1 = tf.ones(shape=(10, 2, 1), dtype=tf.float32)
            m0 = tf.ones(shape=(10, 2, 1), dtype=tf.complex64)
            result = spin_echo_module(m0, T1=T1, T2=T2)
            self.assertTupleEqual((10, 4, 1), result.numpy().shape)

        with self.subTest("2 rep input, exp_fac=2, T1-->2, noexp"):
            T1 = tf.ones(shape=(10, 2, 1), dtype=tf.float32)
            m0 = tf.ones(shape=(10, 2, 1), dtype=tf.complex64)
            spin_echo_module = SpinEcho(
                repetition_time=tf.constant([1000., 1200], dtype=tf.float32),
                echo_time=tf.constant([50., 10.], dtype=tf.float32),
                expand_repetitions=False)
            result = spin_echo_module(m0, T1=T1, T2=T2)
            self.assertTupleEqual((10, 2, 1), result.numpy().shape)

    def test_shape_assertion_on_update(self):
        spin_echo_module = SpinEcho(repetition_time=1000., echo_time=50,
                                    expand_repetitions=True)
        spin_echo_module.TE.assign([10, 20.])
        self.assertRaises(AssertionError, spin_echo_module.update)


class TestSaturationRecoveryGRE(unittest.TestCase):
    def test_calculate_trajectory(self):
        m0 = tf.ones(shape=(10, 1, 1), dtype=tf.complex64)
        T1 = tf.ones(shape=(10, 1, 1), dtype=tf.float32)

        spoiled_gre = SaturationRecoveryGRE(repetition_time=1000., flip_angle=15.,
                                            saturation_delay=150., n_profiles_to_k0=21,
                                            expand_repetitions=True)
        a = spoiled_gre(m0, T1=T1)


class TestBSSFP(unittest.TestCase):

    def test_input_shape(self):
        bssfp_module = BSSFP(flip_angle=tf.constant([15., ]), echo_time=[5, ],
                             repetition_time=[10, ], expand_repetitions=True)
        m0 = tf.ones(shape=(10, 1, 1), dtype=tf.complex64)
        T1 = tf.ones(shape=(10, 1, 1), dtype=tf.float32)
        T2 = tf.ones(shape=(10, 1, 1), dtype=tf.float32)
        T2star = tf.ones(shape=(10, 1, 1), dtype=tf.float32)
        offres = tf.zeros(shape=(10, 1, 1), dtype=tf.float32)

        a = bssfp_module(m0, T1=T1, T2=T2, T2star=T2star, off_res=offres)

    def test_banding(self):
        TR = 6  # ms
        theta_grid = tf.range(-2 * np.pi, 2 * np.pi, np.pi / 50, dtype=tf.float32) / TR

        sequence_module = BSSFP(flip_angle=np.array([35, ]), echo_time=[3, ],
                                repetition_time=[TR, ], expand_repetitions=True)
        inp1 = dict(signal_tensor=tf.ones([1, 1, 1], dtype=tf.complex64), T1=tf.ones([1, 1, 1] ,dtype=tf.float32) * 600,
                    T2=tf.ones([1, 1, 1], dtype=tf.float32) * 100, T2star=tf.ones([1, 1, 1], dtype=tf.float32) * 1000,
                    off_res=tf.reshape(theta_grid, [1, -1, 1]))
        inp2 = dict(signal_tensor=tf.ones([1, 1, 1], dtype=tf.complex64), T1=tf.ones([1, 1, 1], dtype=tf.float32) * 900,
                    T2=tf.ones([1, 1, 1], dtype=tf.float32) * 50, T2star=tf.ones([1, 1, 1], dtype=tf.float32) * 1000,
                    off_res=tf.reshape(theta_grid, [1, -1, 1]))

        res1 = sequence_module(**inp1)
        res2 = sequence_module(**inp2)
        f, a = plt.subplots(1, 1, figsize=(10, 3))
        a.plot(theta_grid.numpy(), tf.abs(tf.reshape(res1, -1)), label="T2/T1: 100/600")
        a.plot(theta_grid.numpy(), tf.abs(tf.reshape(res2, -1)), label="T2/T1: 50/900")
        f.suptitle("Banding structure"), a.set_xlabel(r"$\theta [rad]$"), a.set_ylabel(
            "Relative signal"), a.legend(), f.tight_layout()
        os.makedirs(f"{os.path.dirname(__file__)}/test_output/contrast", exist_ok=True)
        f.savefig(f"{os.path.dirname(__file__)}/test_output/contrast/BSSFP_banding.png")
