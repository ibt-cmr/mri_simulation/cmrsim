import unittest
import tensorflow as tf

from cmrsim.analytic.contrast.t2_star import StaticT2starDecay


class TestInputShapes(unittest.TestCase):
    def setUp(self) -> None:
        sampling_times = tf.range(0., 5., delta=0.1, dtype=tf.float32)
        self.n_sampling_points = int(tf.size(sampling_times))
        self.sampling_times = sampling_times

    def test_correct_input_shapes(self):
        """ Checks all possible correct combinations of shapes for the t2_star map and the signal
        tensor input """

        for n_sampling_points in [1, self.n_sampling_points]:
            m_transverse = tf.ones((1000, 10, n_sampling_points), tf.complex64)
            t2_star_map = tf.random.normal((1000, 1, 1), mean=2., stddev=0.5)

            self.t2_module = StaticT2starDecay(sampling_times=self.sampling_times,
                                               expand_repetitions=False)
            with self.subTest(f"expand: False, samples={n_sampling_points}, "
                              f"reps={self.t2_module.sampling_times.read_value().shape[0]}"):

                traget_shape = (1000, 10, self.n_sampling_points)
                result = self.t2_module(m_transverse, T2star=t2_star_map)
                self.assertTrue(all(i == j for i, j in zip(traget_shape, result.shape)))

            self.t2_module = StaticT2starDecay(
                sampling_times=tf.tile(self.sampling_times[tf.newaxis], [10, 1]),
                expand_repetitions=False)
            with self.subTest(f"expand: False, samples={n_sampling_points}, "
                              f"reps={self.t2_module.sampling_times.read_value().shape[0]}"):
                traget_shape = (1000, 10, self.n_sampling_points)
                result = self.t2_module(m_transverse, T2star=t2_star_map)
                self.assertTrue(all(i == j for i, j in zip(traget_shape, result.shape)))

            self.t2_module = StaticT2starDecay(
                sampling_times=tf.tile(self.sampling_times[tf.newaxis], [10, 1]),
                expand_repetitions=True)
            with self.subTest(f"expand: True, samples={n_sampling_points}, "
                              f"reps={self.t2_module.sampling_times.read_value().shape[0]}"):

                traget_shape = (1000, 100, self.n_sampling_points)
                result = self.t2_module(m_transverse, T2star=t2_star_map)
                self.assertTrue(all(i == j for i, j in zip(traget_shape, result.shape)))
