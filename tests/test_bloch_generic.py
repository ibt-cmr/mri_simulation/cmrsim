import unittest
from collections import OrderedDict
from copy import deepcopy

import numpy as np
import tensorflow as tf
tf.config.run_functions_eagerly(True)

from pint import Quantity
import cmrseq

import cmrsim


class TestGenericBlochModule(unittest.TestCase):
    def setUp(self) -> None:
        self.n_reps = 10
        self.n_steps = 100
        self.time = tf.range(0., self.n_steps, dtype=tf.float32) * 0.01
        self.grads = tf.zeros([self.n_reps, self.n_steps, 3], dtype=tf.float32)
        self.rf = tf.zeros((self.n_reps, self.n_steps), dtype=tf.complex64)
        # This adc-definition should result in 12 time-steps where the adc-event is active for
        # at least one repetition, but only three-samples per repetition eventually stored
        adc_on = tf.roll(tf.eye(self.n_reps, self.n_steps, dtype=tf.float32).numpy(), 1, axis=1)
        adc_on += tf.roll(tf.eye(self.n_reps, self.n_steps, dtype=tf.float32).numpy(), 2, axis=1)
        adc_on += tf.roll(tf.eye(self.n_reps, self.n_steps, dtype=tf.float32).numpy(), 3, axis=1)
        adc_phase = tf.zeros_like(adc_on)
        self.adc = tf.stack([adc_on, adc_phase], axis=-1)

        n_particles = 1000
        self.simulation_input = dict(
            magnetization=tf.tile(tf.constant([[1+0*1j, 1+0*1j, 0+0*1j]], dtype=tf.complex64), [n_particles, 1]),
            T1=tf.ones([n_particles, ], dtype=tf.float32),
            T2=tf.ones([n_particles, ], dtype=tf.float32),
            M0=tf.ones([n_particles, ], dtype=tf.float32),
            initial_position=tf.random.normal([n_particles, 3], 0., 0.005),
            off_res=tf.random.normal([n_particles, 1], 0, 1, dtype=tf.float32),
            omega_t2s=tf.random.normal([n_particles, ], 0., 0.1, dtype=tf.float32)
        )
        self.n_particles = n_particles

    def test_input_shapes(self):

        def _test_sequential_call(module: cmrsim.bloch.GeneralBlochOperator):
            module.reset()
            m, r = module(repetition_index=tf.constant(0),
                          run_parallel=False, **self.simulation_input)
            temp_dict = dict(**self.simulation_input)
            temp_dict["magnetization"] = m
            temp_dict["initial_position"] = r
            m, r = module(repetition_index=tf.constant(1), run_parallel=False, **temp_dict)
            # print(tf.abs(tf.stack(module.time_signal_acc)) > 0)
            self.assertTrue(all([i == j for i, j in zip(m.shape, [self.n_particles, 3])]))
            self.assertTrue(m.dtype is tf.complex64)
            self.assertTrue(r.dtype is tf.float32)
            self.assertTrue(np.prod(np.abs(tf.stack(module.time_signal_acc)) > 0, axis=1)[0])
            self.assertTrue(np.prod(np.abs(tf.stack(module.time_signal_acc)) > 0, axis=1)[1])
            self.assertTrue(not np.sum(np.abs(tf.stack(module.time_signal_acc)) > 0, axis=1)[2])

        def _test_parallel_call(module: cmrsim.bloch.GeneralBlochOperator):
            module.reset()
            m, r = module(run_parallel=True, **self.simulation_input)
            self.assertTrue(all([i == j for i, j in
                                 zip(m.shape, [self.n_reps, self.n_particles, 3])]))
            self.assertTrue(m.dtype is tf.complex64)
            self.assertTrue(r.dtype is tf.float32)
            # print(tf.abs(tf.stack(module.time_signal_acc)))
            second_iteration_input = deepcopy(self.simulation_input)
            [second_iteration_input.pop(k) for k in ("magnetization", "initial_position")]
            m, r = module(run_parallel=True, magnetization=m, initial_position=r,
                          **second_iteration_input)
            self.assertTrue(all([i == j for i, j in
                                 zip(m.shape, [self.n_reps, self.n_particles, 3])]))
            self.assertTrue(m.dtype is tf.complex64)
            self.assertTrue(r.dtype is tf.float32)
            self.assertTrue(np.prod(np.abs(tf.stack(module.time_signal_acc)) > 0, axis=(0, 1)))

        # test __call__ without submodules
        module_ = cmrsim.bloch.GeneralBlochOperator(name="test", gamma=1., time_grid=self.time,
                                                    gradient_waveforms=self.grads,
                                                    rf_waveforms=self.rf, adc_events=self.adc)
        with self.subTest("No submodules, sequential simulation"):
            _test_sequential_call(module_)

        with self.subTest("No submodules, parallel simulation"):
            _test_parallel_call(module_)

        # test __call__ with submodules
        submodules = (cmrsim.bloch.submodules.OffResonance(1),
                      cmrsim.bloch.submodules.T2starDistributionModel(),
                      cmrsim.bloch.submodules.ConcomitantFields(1, 1))
        module_ = cmrsim.bloch.GeneralBlochOperator(name="test", gamma=1., time_grid=self.time,
                                                    gradient_waveforms=self.grads,
                                                    rf_waveforms=self.rf,
                                                    adc_events=self.adc,
                                                    submodules=submodules)
        with self.subTest("With all submodules sequential call"):
            _test_sequential_call(module_)

        with self.subTest("With all submodule parallel call"):
            _test_parallel_call(module_)

    def test_bulk_phase(self):
        """ Sets up a bipolar gradient module and a bunch of particle that move with constant
        velocity. Checks if the phase of the particles is modified correctly and the
        longitudinal relaxation is correct."""
        # Set up sequence & module
        system_specs = cmrseq.SystemSpec(max_slew=Quantity(200., "mT/m/ms"),
                                         max_grad=Quantity(80., "mT/m"),
                                         grad_raster_time=Quantity(0.005, "ms"),
                                         rf_raster_time=Quantity(0.005, "ms"))
        seq = cmrseq.seqdefs.velocity.bipolar(system_specs=system_specs,
                                              venc=Quantity(200, "cm/s"),
                                              duration=Quantity(2.5, "ms"),
                                              direction=np.array([-1., 0., 0.]))
        t, gridded_seq = seq.gradients_to_grid()
        wf = gridded_seq.T
        dt = system_specs.grad_raster_time.m_as("ms")
        module = cmrsim.bloch.GeneralBlochOperator("test_grad",
                                                   gamma=system_specs.gamma_rad.m_as("rad/ms/mT"),
                                                   time_grid=t,
                                                   gradient_waveforms=wf[np.newaxis])

        class TrajMod(tf.Module):
            def increment_particles(self, particle_positions: tf.Tensor, dt: tf.Tensor):
                v = tf.constant(Quantity([25, 0., 0.], "cm/s").m_as("m/ms"),
                                dtype=tf.float32, shape=(1, 3))
                return particle_positions + v * dt, dict()
    


        with self.subTest("Mean Phase increase due to motion in direction of bipolar gradient"):
            # Set up phantom
            n_particles = 100
            r_init = tf.random.normal((n_particles, 3), 0., 0.005)
            m = tf.tile(tf.constant([[1., 0., 0.], ], dtype=tf.complex64), [n_particles, 1])
            properties = OrderedDict(
                magnetization=m,
                T1=tf.ones((n_particles,), dtype=tf.float32) * Quantity(1000, "ms").m,
                T2=tf.ones((n_particles,), dtype=tf.float32) * Quantity(500, "ms").m,
                M0=tf.ones((n_particles,), dtype=tf.float32) * 10)

            # call module
            m_new, r = module(trajectory_module=TrajMod(),
                              initial_position=r_init,
                              repetition_index=0,
                              run_parallel=True, **properties)
            
            mean_phase = tf.reduce_mean(tf.math.angle(m_new[0, :, 0]))
            relaxation_factor = 1. - np.exp(-(wf.shape[0]-1)*dt/1000)  # must match T1 definition
            self.assertGreater(tf.abs(mean_phase), tf.reduce_mean(tf.math.angle(m[:, 0])))
            self.assertTrue(np.allclose(tf.math.angle(m_new[0, :, 0]).numpy(), mean_phase.numpy(), atol=1e-3))
            self.assertTrue(np.allclose(tf.abs(m_new[0, :, 2]), relaxation_factor, atol=1e-4))

