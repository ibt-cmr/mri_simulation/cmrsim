import pyvista as pv
import numpy as np 

mesh_path='steady_stenosis.vtk'
mesh = pv.read(mesh_path)

Coords_0  = mesh.points

image_box = [(np.max(Coords_0[:,0])-np.min(Coords_0[:,0])),\
        (np.max(Coords_0[:,1])-np.min(Coords_0[:,1])),\
        (np.max(Coords_0[:,2])-np.min(Coords_0[:,2]))]

orig_image =[np.min(Coords_0,axis=0)[0],\
        np.min(Coords_0,axis=0)[1],\
        np.min(Coords_0,axis=0)[2]]

image_sizex=0.5e-3
image_sizey=0.5e-3
image_sizez=0.5e-3

nx = int(image_box[0]/image_sizex)
ny = int(image_box[1]/image_sizey)
nz = int(image_box[2]/image_sizez)

res_x = image_box[0]/nx
res_y = image_box[1]/ny
res_z = image_box[2]/nz

dims=(nx, ny, nz)
spacing=(res_x,res_y,res_z)
origin=(orig_image[0],orig_image[1],orig_image[2])
image = pv.UniformGrid(dims,spacing,origin)

mesh=mesh.cell_data_to_point_data()
probed_image=image.sample(mesh)
array_data=probed_image.point_arrays
probed_image.clear_arrays()
probed_image['U']=array_data['U']
probed_image=probed_image.point_data_to_cell_data()
probed_image.save(mesh_path.rsplit('.vtk')[0]+'.vti')