import unittest
import os

import tensorflow as tf
import numpy as np

from cmrsim.utils.coordinates import get_static_2d_centered_coordinates
from cmrsim.analytic.contrast import CoilSensitivity, BirdcageCoilSensitivities

import tests.phantoms




class TestInputShape(unittest.TestCase):
    def setUp(self) -> None:
        """ Set up dummy sensitivity values.

        :return:
        """
        n_repetitions, n_samples, n_coils = 13, 1, 8
        nx, ny, nz, = 150, 130, 3
        signal_tensor = tf.cast(tf.round(tf.random.uniform((nx * ny, 1, 1), seed=0)), tf.complex64)
        non_zero_indices = tf.where(tf.abs(signal_tensor) > 0)[:, 0]
        signal_tensor = tf.gather(signal_tensor, non_zero_indices, axis=0)
        signal_tensor = tf.tile(signal_tensor, [1, n_repetitions, n_samples])

        r_vectors = tf.reshape(get_static_2d_centered_coordinates((nx, ny), (0.1, 0.1)),
                               [-1, 1, 1, 3])
        r_vectors = tf.gather(r_vectors, non_zero_indices, axis=0)
        r_vectors = tf.tile(r_vectors, [1, n_repetitions, n_samples, 1])

        single_map = tf.reshape(tf.range(nx * ny * nz, dtype=tf.float32), [nx, ny, nz])
        coil_sensitivities = tf.stack([single_map * i for i in range(1, n_coils + 1)], axis=-1)
        coil_sensitivities = tf.cast(coil_sensitivities, tf.complex64)        #
        map_dimensions = np.array(((-0.75, 0.75), (-0.75, 0.75), (0., 1.)))

        self.s = CoilSensitivity(tf.transpose(coil_sensitivities, [1, 0, 2, 3]), map_dimensions)
        self.signal_tensor = signal_tensor
        self.r_vectors = r_vectors

    def test_static_during_readout(self):
        import time
        dataset = tf.data.Dataset.from_tensor_slices((self.signal_tensor, self.r_vectors))
        dataset = dataset.batch(500)
        loop_start = time.perf_counter()
        print("Start loop")
        for idx, batch in dataset.enumerate():
            iteration_start = time.perf_counter()
            m, r = batch
            result = self.s(m, r_vectors=r)
            print(f"\r {idx}, Total duration: {time.perf_counter() - loop_start: 1.4f},  "
                  f"Iteration duration: {time.perf_counter() - iteration_start: 1.4f},  {result.shape}",
                  end="")


class TestValues(unittest.TestCase):
    def setUp(self) -> None:
        self.sensitivity_maps = np.load(f'{os.path.dirname(__file__)}/'
                                        f'resources/coil_sensitivities.npy').transpose([1, 2, 3, 0])

        nx, ny, nz, ncoils = self.sensitivity_maps.shape
        map_dimensions = ((-0.05, 0.05), (-0.05, 0.05), (0.0, 0.1))

        # Transpose image to match mesh-grid coordinate definition
        _, m0, r = tests.phantoms.define_cylinder_phantom(fov=(0.0999, 0.0999), map_size=(nx, ny))
        self.m0_ref = m0
        self.signal_tensor_circ = tf.complex(tf.reshape(m0, [-1, 1, 1]), 0.)
        self.r_vectors_circ = tf.reshape(r, [-1, 1, 1, 3])
        self.signal_tensor_const = tf.ones(((nx * ny * nz), 1, 1), tf.complex64)
        self.r_vectors_const = get_static_2d_centered_coordinates((nx, ny), (0.1, 0.1),
                                                                  return_2d=False)
        self.module = CoilSensitivity(self.sensitivity_maps.transpose([1, 0, 2, 3]), map_dimensions)
        self.module.update()
        self.assertEqual(self.module.look_up_map3d.read_value().shape[-1], self.module._n_channels)

    def test_circle_plot(self):
        dataset = tf.data.Dataset.from_tensor_slices((self.signal_tensor_circ, self.r_vectors_circ))
        n_iterations = int(np.ceil(self.signal_tensor_const.shape[0] / 500))
        result = tf.TensorArray(tf.complex64, size=n_iterations)
        for idx, batch in dataset.batch(500).enumerate():
            m, r = batch
            result = result.write(idx, self.module(m, r_vectors=r))
        result = result.concat().numpy()
        result = np.reshape(result, [-1, 8])

        import matplotlib.pyplot as plt
        f, (a1, a2) = plt.subplots(2, 8)
        result_2d = np.abs(result).reshape(*self.sensitivity_maps.shape[0:2], 8)
        [a1[i].imshow(result_2d[..., i]) for i in range(8)]
        [a2[i].imshow(np.abs(self.sensitivity_maps[:, :, 0, i] * self.m0_ref)) for i in range(8)]
        [_.axis("off") for _ in [*a1.flatten(), *a2.flatten()]]
        f.set_size_inches(16, 5), f.tight_layout()

        os.makedirs(f"{os.path.dirname(__file__)}/test_output/contrast", exist_ok=True)
        f.savefig(f"{os.path.dirname(__file__)}/test_output/contrast/coils_circle.png")

    def test_constant_input(self):
        """ Uses constant transverse magnetization on the same resolution as the coil_sensitivity
        maps, multiplies with
        all vectors and checks if the result matches the values of the coil_sensitivity maps.
        Possible reasons to fail:
            - positional vectors are not aligned with lookup assumption
        """
        dataset = tf.data.Dataset.from_tensor_slices((self.signal_tensor_const,
                                                      self.r_vectors_const))
        n_iterations = int(np.ceil(self.signal_tensor_const.shape[0] / 500))
        result = tf.TensorArray(tf.complex64, size=n_iterations)
        for idx, batch in dataset.batch(500).enumerate():
            m, r = batch
            result = result.write(idx, self.module(m, r_vectors=r))
        result = result.concat().numpy()
        result = np.reshape(result, [-1, 8])

        result_2d = result.reshape(*self.sensitivity_maps.shape[0:2], 8)
        plot_2d1 = np.abs(result_2d)
        plot_2d2 = np.abs(self.sensitivity_maps[:, :, 0])
        plot_2d3 = np.abs(result_2d - self.sensitivity_maps[:, :, 0])

        import matplotlib.pyplot as plt
        f, (a1, a2, a3) = plt.subplots(3, 8)
        [a1[i].imshow(plot_2d1[..., i], vmin=0, vmax=0.1) for i in range(8)]
        [a2[i].imshow(plot_2d2[..., i], vmin=0, vmax=0.1) for i in range(8)]
        [a3[i].imshow(plot_2d3[..., i], vmin=0, vmax=0.1) for i in range(8)]
        [[_.axis("off") for _ in a.flatten()] for a in (a1, a2, a3)]
        f.set_size_inches(16, 8), f.tight_layout()
        os.makedirs(f"{os.path.dirname(__file__)}/test_output/contrast", exist_ok=True)
        f.savefig(f"{os.path.dirname(__file__)}/test_output/contrast/coils_const.png")
        self.assertTrue(np.allclose(plot_2d3, 0., atol=0.01))


class TestValuesBirdcage(unittest.TestCase):
    def setUp(self) -> None:
        shape = nx, ny, nz, ncoils = 100, 150, 1, 8
        map_dimensions = ((-0.05, 0.05), (-0.05, 0.05), (0.0, 0.1))

        # Transpose image to match mesh-grid coordinate definition
        _, m0, r = tests.phantoms.define_cylinder_phantom(fov=(0.0999, 0.0999), map_size=(nx, ny))
        # r = r[..., (0, 1, 2)]
        self.m0_ref = m0
        self.signal_tensor_circ = tf.complex(tf.reshape(m0, [-1, 1, 1]), 0.)
        self.r_vectors_circ = tf.reshape(r, [-1, 1, 1, 3])
        self.signal_tensor_const = tf.ones(((nx * ny * nz), 1, 1), tf.complex64)
        self.r_vectors_const = get_static_2d_centered_coordinates((nx, ny), (0.1, 0.1),
                                                                  return_2d=False).numpy()
        self.module = BirdcageCoilSensitivities((nx, ny, nz, ncoils), map_dimensions, 1.5)
        self.module.update()
        self.assertEqual(self.module.look_up_map3d.read_value().shape[-1], self.module._n_channels)

        self.sensitivity_maps = self.module.coil_maps.numpy().transpose(1, 0, 2, 3)

    def test_circle_plot(self):
        dataset = tf.data.Dataset.from_tensor_slices((self.signal_tensor_circ, self.r_vectors_circ))
        n_iterations = int(np.ceil(self.signal_tensor_const.shape[0] / 500))
        result = tf.TensorArray(tf.complex64, size=n_iterations)
        for idx, batch in dataset.batch(500).enumerate():
            m, r = batch
            result = result.write(idx, self.module(m, r_vectors=r))
        result = result.concat().numpy()
        result = np.reshape(result, [-1, 8])

        import matplotlib.pyplot as plt
        f, (a1, a2) = plt.subplots(2, 8)
        result_2d = np.abs(result).reshape(*self.sensitivity_maps.shape[0:2][::], 8)
        [a1[i].imshow(result_2d[..., i]) for i in range(8)]
        [a2[i].imshow(np.abs(self.sensitivity_maps[:, :, 0, i] * self.m0_ref)) for i in range(8)]
        [_.axis("off") for _ in [*a1.flatten(), *a2.flatten()]]
        f.set_size_inches(16, 5), f.tight_layout()

        os.makedirs(f"{os.path.dirname(__file__)}/test_output/contrast", exist_ok=True)
        f.savefig(f"{os.path.dirname(__file__)}/test_output/contrast/coils_circle_birdcage.png")

    def test_constant_input(self):
        """ Uses constant transverse magnetization on the same resolution as the coil_sensitivity
        maps, multiplies with
        all vectors and checks if the result matches the values of the coil_sensitivity maps.
        Possible reasons to fail:
            - positional vectors are not aligned with lookup assumption
        """
        dataset = tf.data.Dataset.from_tensor_slices((self.signal_tensor_const,
                                                      self.r_vectors_const))
        n_iterations = int(np.ceil(self.signal_tensor_const.shape[0] / 500))
        result = tf.TensorArray(tf.complex64, size=n_iterations)
        for idx, batch in dataset.batch(500).enumerate():
            m, r = batch
            result = result.write(idx, self.module(m, r_vectors=r))
        result = result.concat().numpy()
        result = np.reshape(result, [-1, 8])

        result_2d = result.reshape(*self.sensitivity_maps.shape[0:2][::], 8)
        plot_2d1 = np.abs(result_2d)
        plot_2d2 = np.abs(self.sensitivity_maps[:, :, 0])
        plot_2d3 = np.abs(result_2d - self.sensitivity_maps[:, :, 0])

        import matplotlib.pyplot as plt
        f, (a1, a2, a3) = plt.subplots(3, 8)
        [a1[i].imshow(plot_2d1[..., i], vmin=0, vmax=.8) for i in range(8)]
        [a2[i].imshow(plot_2d2[..., i], vmin=0, vmax=.8) for i in range(8)]
        [a3[i].imshow(plot_2d3[..., i], vmin=0, vmax=.1) for i in range(8)]
        [[_.axis("off") for _ in a.flatten()] for a in (a1, a2, a3)]
        f.set_size_inches(16, 8), f.tight_layout()
        os.makedirs(f"{os.path.dirname(__file__)}/test_output/contrast", exist_ok=True)
        f.savefig(f"{os.path.dirname(__file__)}/test_output/contrast/coils_const_birdcage.png")
        self.assertTrue(np.allclose(plot_2d3, 0., atol=0.1))
