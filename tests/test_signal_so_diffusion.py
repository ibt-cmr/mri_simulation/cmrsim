import unittest
import itertools

import tensorflow as tf
import numpy as np

from cmrsim.analytic.contrast import GaussianDiffusionTensor


class TestInputShapes(unittest.TestCase):
    def setUp(self) -> None:
        random_vectors = tf.random.uniform((6, 3))
        random_vectors = random_vectors / tf.linalg.norm(random_vectors, axis=1, keepdims=True)
        b_vectors = tf.reshape(
            tf.einsum('n, ij -> nij', tf.sqrt(tf.range(0, 1000, 500, dtype=tf.float32)),
                      random_vectors), [-1, 3])
        self.n_b_vectors = tf.shape(b_vectors)[0]
        self.diffusion_module_exp = GaussianDiffusionTensor(b_vectors=b_vectors,
                                                            expand_repetitions=True)
        self.diffusion_module = GaussianDiffusionTensor(b_vectors=b_vectors,
                                                        expand_repetitions=False)
        self.n_parts = 30

    def test_correct_input_shapes(self):
        """ """
        shape_iterator = itertools.product((1, 4), (1, 49))
        for previous_repetions, previous_k_space_samples in shape_iterator:
            with self.subTest(f"Expand dimensions {previous_repetions, previous_k_space_samples}"):
                m_transverse = tf.ones((self.n_parts, previous_repetions,
                                        previous_k_space_samples), tf.complex64)
                diffusion_tensors = tf.eye(3, 3, batch_shape=[self.n_parts, 1, 1])
                target_shape = (self.n_parts, previous_repetions * self.n_b_vectors,
                                previous_k_space_samples)

                with tf.device('CPU:0'):
                    result = self.diffusion_module_exp(m_transverse, diffusion_tensor=diffusion_tensors)
                    self.assertTrue(all(i == j for i, j in zip(target_shape, result.shape)))

        shape_iterator = itertools.product([int(self.n_b_vectors), ],  (1, 49))
        for previous_repetions, previous_k_space_samples in shape_iterator:
            with self.subTest(f"No Expansion {previous_repetions, previous_k_space_samples}"):
                m_transverse = tf.ones((self.n_parts, previous_repetions,
                                        previous_k_space_samples), tf.complex64)
                diffusion_tensors = tf.eye(3, 3, batch_shape=[self.n_parts, 1, 1])
                target_shape = (self.n_parts, self.n_b_vectors, previous_k_space_samples)

                with tf.device('CPU:0'):
                    result = self.diffusion_module(m_transverse, diffusion_tensor=diffusion_tensors)
                    self.assertTrue(all(i == j for i, j in zip(target_shape, result.shape)))


class TestValues(unittest.TestCase):
    def setUp(self) -> None:
        directions = tf.eye(3, 3)
        self.b_values = tf.range(0, 1000, 100, dtype=tf.float32)
        b_vectors = tf.reshape(tf.einsum('n, ij -> nij', tf.sqrt(self.b_values), directions), [-1, 3])
        self.n_b = tf.shape(self.b_values)[0]
        self.diffusion_module = GaussianDiffusionTensor(b_vectors=b_vectors,
                                                        expand_repetitions=True)

    def test_decreasing_values(self):
        """Checks  if the calcutation performed by the diffusion module is as
        expected for a trivial set of directions and diffusion tensors.
        **Important**: The way the b-vectors are defined implies the unpacking in
         values/directions to follow: (..., nb*ndir, ...) -> (..., nb, ndir, ...)
        """
        n_parts = 9
        diff_tensors = tf.einsum('vrkij, i -> vrkij',
                                 tf.eye(3, 3, batch_shape=[n_parts, 1, 1]),
                                 tf.constant((1, 2, 3), dtype=tf.float32)) * 10e-4
        m0 = tf.ones((n_parts, 1, 1), tf.complex64)

        reference = np.array([[tf.cast(tf.exp(-a*b).numpy(), tf.complex64) for b in self.b_values]
                              for a in (10e-4, 20e-4, 30e-4)]).T
        decayed_m0 = self.diffusion_module(m0, diffusion_tensor=diff_tensors)
        result = decayed_m0.numpy().reshape((n_parts, self.n_b, 3))
        comparison = [np.allclose(res_i, ref) for res in result
                      for res_i, ref in zip(res, reference)]
        self.assertTrue(all(i for i in comparison))
