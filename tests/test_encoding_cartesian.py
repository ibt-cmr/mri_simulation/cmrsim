import unittest
import os

import numpy as np
import matplotlib.pyplot as plt

from cmrsim.analytic.encoding import SingleLinePerShot, EPI
plt.close("all")


def _plot_trajectory(trajectory: np.ndarray, times: np.ndarray, name: str) -> None:
    """ Plots trajectory and saves it to file

    :param trajectory: (-1, 3)
    :param times: (-1, )
    :param name: sup-title of figure
    :returns: None
    """

    f, (a1, a2) = plt.subplots(1, 2)
    tx, ty = np.arange(0, len(times), 1), times
    a1.scatter(tx, ty, c=times, cmap="inferno")
    a1.grid(True), a1.set_xlabel("Index"), a1.set_ylabel("$ t / ms$")
    a1.set_title("Sampling times")

    x, y = trajectory[:, 0:2].T
    a2.scatter(x, y, c=times, cmap="inferno")
    a2.grid(True), a2.set_xlabel(r"$\vec{k}_x$"), a2.set_ylabel(r"$\vec{k}_y$")
    a2.set_title("K-space vectors 2D")

    for n in range(len(times)):
        a1.annotate(str(n), (tx[n], ty[n]))
        a2.annotate(str(n), (x[n], y[n]))

    f.set_size_inches(16, 8)
    f.tight_layout()

    output_dir = os.path.abspath(f"{os.path.dirname(__file__)}/test_output/encoding/")
    os.makedirs(output_dir, exist_ok=True)
    f.savefig(f"{output_dir}/{name}.png")


class TestSingleLinePerShot(unittest.TestCase):
    def test_sampling_time_calculation(self):
        """ Construct single line per shot """
        encoding_module = SingleLinePerShot(field_of_view=(0.34, 0.11),
                                            sampling_matrix_size=(13, 7),
                                            repetition_time=2., read_out_duration=1.,
                                            acquisition_start=0., absolute_noise_std=0.)
        k_of_t = encoding_module.sampling_times.numpy()

        # check if sampling times per line are identical
        temp = k_of_t.reshape(7, 13) - k_of_t[:13][: np.newaxis]
        self.assertEqual(np.sum(temp), 0.)

    def test_plot_trajectory(self):

        for samp_mat in [(13, 7), (12, 7), (13, 8), (12, 8)]:
            module = SingleLinePerShot(field_of_view=(0.34, 0.11),
                                       sampling_matrix_size=samp_mat,
                                       repetition_time=2., read_out_duration=1.,
                                       acquisition_start=0., absolute_noise_std=0.)
            name = f"single_line_trajectory_{samp_mat}"
            _plot_trajectory(module.k_space_vectors.numpy(),
                             module.sampling_times.numpy().flatten(),
                             name=name)


class TestEPI(unittest.TestCase):
    def test_plot_trajectory_single_segment(self):
        for samp_mat in [(13, 7), (12, 7), (13, 8), (12, 8)]:
            module = EPI(field_of_view=(0.31, 0.112), sampling_matrix_size=samp_mat,
                         bandwidth_per_pixel=1300., blip_duration=1., k_space_segments=1,
                         acquisition_start=-25.7692, absolute_noise_std=0.)
            name = f"EPI_{samp_mat}"
            _plot_trajectory(module.k_space_vectors.numpy(),
                             module.sampling_times.numpy().flatten(),
                             name=name)

    def test_plot_trajectory_multi_segment(self):
        for samp_mat in [(13, 7), (12, 7), (13, 8), (12, 8)]:
            module = EPI(field_of_view=(0.31, 0.112), sampling_matrix_size=samp_mat,
                         bandwidth_per_pixel=1300., blip_duration=1., k_space_segments=samp_mat[1],
                         acquisition_start=-25.7692, absolute_noise_std=0.)
            name = f"EPI_{samp_mat}_seg{samp_mat[1]}"
            _plot_trajectory(module.k_space_vectors.numpy(),
                             module.sampling_times.numpy().flatten(),
                             name=name)


