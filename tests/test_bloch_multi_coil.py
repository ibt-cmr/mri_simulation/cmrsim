import unittest
from collections import OrderedDict
from copy import deepcopy

import numpy as np
import tensorflow as tf
tf.config.run_functions_eagerly(True)

import cmrsim


class TestParallelReceiveBlochSimulation(unittest.TestCase):
    def setUp(self) -> None:
        self.n_reps = 10
        self.n_steps = 100
        self.time = tf.range(0., self.n_steps, dtype=tf.float32) * 0.01
        self.grads = tf.zeros([self.n_reps, self.n_steps, 3], dtype=tf.float32)
        self.rf = tf.zeros((self.n_reps, self.n_steps), dtype=tf.complex64)
        # This adc-definition should result in 12 time-steps where the adc-event is active for
        # at least one repetition, but 6 samples per repetition eventually stored
        adc_on = tf.roll(tf.eye(self.n_reps, self.n_steps,
                                dtype=tf.float32).numpy(), 1, axis=1)
        for i in range(2, 12, 2):
            adc_on += tf.roll(tf.eye(self.n_reps, self.n_steps,
                                     dtype=tf.float32).numpy(), i, axis=1)
        adc_phase = tf.zeros_like(adc_on)
        self.adc = tf.stack([adc_on, adc_phase], axis=-1)

        n_particles = 1000
        self.simulation_input = dict(
            magnetization=tf.tile(
                tf.constant([[1 + 0 * 1j, 1 + 0 * 1j, 0 + 0 * 1j]], dtype=tf.complex64),
                [n_particles, 1]),
            T1=tf.ones([n_particles, ], dtype=tf.float32),
            T2=tf.ones([n_particles, ], dtype=tf.float32),
            M0=tf.ones([n_particles, ], dtype=tf.float32),
            initial_position=tf.random.normal([n_particles, 3], 0., 0.005),
            off_res=tf.random.normal([n_particles, 1], 0, 1, dtype=tf.float32),
            omega_t2s=tf.random.normal([n_particles, ], 0., 0.1, dtype=tf.float32)
        )
        self.n_particles = n_particles

        self.coil_module = cmrsim.analytic.contrast.CoilSensitivity(
                            coil_sensitivities=np.ones([1, 20, 20, 20, 3], dtype=np.complex64),
                            map_dimensions=np.array([[-0.1, 0.1], [-0.1, 0.1], [-0.1, 0.1]]))

    def test_input_shapes(self):
        def _test_sequential_call(module: cmrsim.bloch.ParallelReceiveBlochOperator):
            module.reset()
            m, r = module(repetition_index=tf.constant(0),
                          run_parallel=False, **self.simulation_input)
            temp_dict = dict(**self.simulation_input)
            temp_dict["magnetization"] = m
            temp_dict["initial_position"] = r
            m, r = module(repetition_index=tf.constant(1), run_parallel=False, **temp_dict)
            # print(tf.abs(tf.stack(module.time_signal_acc)) > 0)
            self.assertTrue(all([i == j for i, j in zip(m.shape, [self.n_particles, 3])]))
            self.assertTrue(m.dtype is tf.complex64)
            self.assertTrue(r.dtype is tf.float32)
            self.assertTrue(np.prod(np.abs(tf.stack(module.time_signal_acc)) > 0, axis=(1, 2))[0])
            self.assertTrue(np.prod(np.abs(tf.stack(module.time_signal_acc)) > 0, axis=(1, 2))[1])
            self.assertTrue(not np.sum(np.abs(tf.stack(module.time_signal_acc)) > 0, axis=(1, 2))[2])

        def _test_parallel_call(module: cmrsim.bloch.ParallelReceiveBlochOperator):
            module.reset()
            m, r = module(run_parallel=True, **self.simulation_input)
            self.assertTrue(all([i == j for i, j in
                                 zip(m.shape, [self.n_reps, self.n_particles, 3])]))
            self.assertTrue(m.dtype is tf.complex64)
            self.assertTrue(r.dtype is tf.float32)

            second_iteration_input = deepcopy(self.simulation_input)
            [second_iteration_input.pop(k) for k in ("magnetization", "initial_position")]
            m, r = module(run_parallel=True, magnetization=m, initial_position=r,
                          **second_iteration_input)
            # print(tf.abs(tf.stack(module.time_signal_acc)))
            self.assertTrue(all([i == j for i, j in
                                 zip(m.shape, [self.n_reps, self.n_particles, 3])]))
            self.assertTrue(m.dtype is tf.complex64)
            self.assertTrue(r.dtype is tf.float32)
            self.assertTrue(m.dtype is tf.complex64)
            self.assertTrue(r.dtype is tf.float32)
            self.assertTrue(np.prod(np.abs(tf.stack(module.time_signal_acc)) > 0, axis=(0, 1, 2)))

        # test __call__ without submodules
        module_ = cmrsim.bloch.ParallelReceiveBlochOperator(name="test", gamma=1.,
                                                            coil_module=self.coil_module,
                                                            time_grid=self.time,
                                                            gradient_waveforms=self.grads,
                                                            adc_events=self.adc,
                                                            rf_waveforms=self.rf)
        with self.subTest("No submodules, sequential simulation"):
            _test_sequential_call(module_)

        with self.subTest("No submodules, parallel simulation"):
            _test_parallel_call(module_)

        # test __call__ with submodules
        submodules = (cmrsim.bloch.submodules.OffResonance(1),
                      cmrsim.bloch.submodules.T2starDistributionModel(),
                      cmrsim.bloch.submodules.ConcomitantFields(1, 1),
                      )
        module_ = cmrsim.bloch.ParallelReceiveBlochOperator(name="test", gamma=1.,
                                                            coil_module=self.coil_module,
                                                            time_grid=self.time,
                                                            gradient_waveforms=self.grads,
                                                            adc_events=self.adc,
                                                            rf_waveforms=self.rf,
                                                            submodules=submodules)
        with self.subTest("With all submodules sequential call"):
            _test_sequential_call(module_)

        with self.subTest("With all submodule parallel call"):
            _test_parallel_call(module_)
