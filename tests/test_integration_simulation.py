import unittest
import os

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
tf.config.run_functions_eagerly(True)

import cmrsim.utils
import tests.phantoms
import cmrsim

plt.close("all")


class ExampleAnalyticSimulation(cmrsim.analytic.AnalyticSimulation):
    def build(self):
        n_kx, n_ky = 100, 75

        # Encoding definition / To enable result comparisons, the noise is set to 0 here
        encoding_module = cmrsim.analytic.encoding.EPI(
                                field_of_view=(0.33, 0.3), sampling_matrix_size=(n_kx, n_ky),
                                bandwidth_per_pixel=1300. / 3, blip_duration=0., k_space_segments=1,
                                acquisition_start=-25.7692, absolute_noise_std=0.)

        # Signal module construction
        spinecho_module = cmrsim.analytic.contrast.SpinEcho(echo_time=np.ones((1,)) * 50,
                                                            repetition_time=np.ones((1,)) * 9000,
                                                            expand_repetitions=True)
        forward_model = cmrsim.analytic.CompositeSignalModel(spinecho_module)

        # Reconstruction
        recon_module = cmrsim.reconstruction.Cartesian2D(sample_matrix_size=(n_kx, n_ky),
                                                         padding=None)

        return forward_model, encoding_module, recon_module


class TestCartesianReconstruction(unittest.TestCase):
    def _plot_result(self, result, phantom_ref, coords, filename):
        f, a = plt.subplots(2, 2)
        a = a.flatten()
        a[0].imshow(np.abs(result), cmap="gray", vmax=None, origin="lower")
        a[1].imshow(np.abs(np.angle(result)), cmap="gray", origin="lower")
        a[2].imshow(phantom_ref, origin="lower")
        a[3].scatter(coords[..., 0], coords[..., 1], c=phantom_ref)
        [ax.set_title(t) for ax, t in
         zip(a, ["Magnitude", "Phase", "Phantom Imshow", "Phantom Scatter"])]
        [ax.axis("off") for ax in a]
        f.set_size_inches(10, 10)
        f.tight_layout()
        f.savefig(filename)

    def test_simulate_shepplogan(self):
        output_dir = f"{os.path.dirname(__file__)}/test_output/regression"
        os.makedirs(output_dir, exist_ok=True)
        phantom_dict, phantom_ref, coords = tests.phantoms.define_shepplogan((0.3, 0.29), (100, 75))
        dataset = tests.phantoms.dict_to_base(phantom_dict)
        simulator = ExampleAnalyticSimulation(build_kwargs=dict())
        result = simulator(dataset, voxel_batch_size=1000)
        result = np.squeeze(result)

        self._plot_result(result, phantom_ref, coords, f"{output_dir}/shep_logan_example.jpeg")

    def test_simulate_circles(self):
        output_dir = f"{os.path.dirname(__file__)}/test_output/regression"
        os.makedirs(output_dir, exist_ok=True)
        phantom_dict, phantom_ref, coords = tests.phantoms.define_cylinder_phantom((0.3, 0.29),
                                                                                   (100, 75))
        dataset = tests.phantoms.dict_to_base(phantom_dict)
        simulator = ExampleAnalyticSimulation(build_kwargs=dict())
        result = simulator(dataset, voxel_batch_size=1000)
        result = np.squeeze(result)

        self._plot_result(result, phantom_ref, coords, f"{output_dir}/cricle_example.jpeg")
