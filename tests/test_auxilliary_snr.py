import unittest

import numpy as np
import tensorflow as tf

import cmrsim.utils.snr as cmrsim_snr


class TestComputeNoiseStd(unittest.TestCase):
    """ Tests: cmrsim.utils.snr.compute_noise_std """

    def test_single_coil(self):
        """ Tests the trivial case of having a single image noise less image (1 coil).

        :return:
        """
        image = np.ones((1, 100, 100), dtype=np.complex64)
        correct_stds = np.array([1., 0.1, 0.01])
        target_snr = [1., 10., 100.]
        random_mask = np.around(np.random.uniform(0., 1., size=(100, 100)))

        for mask in [None, random_mask]:
            with self.subTest():
                estimated_stds = cmrsim_snr.compute_noise_std(image, target_snr, coil_sensitivities=None, mask=mask)
                self.assertTrue(np.allclose(correct_stds, estimated_stds))

    def test_multiple_coils(self):
        """ Constructs a set of identical, noise-less images for n coils with pixel values 1.+0.j for all pixels
         and coil_sensitivities that are homogeneous as 1.+0.j and scaled by 1/number_of_coils.
         The correct noise-standard deviations in this special case of data are assumed to match
            sqrt(1/n_coils) / target_snr (for sum of squares reconstruction)
        """
        n_coils = 4
        image = np.ones((n_coils, 100, 100), dtype=np.complex64)
        coil_sensitivities = np.ones_like(image) / n_coils

        image = image * coil_sensitivities

        target_snr = np.array([10., 20., 100.])
        correct_stds = 1/target_snr * np.sqrt(1/n_coils)
        random_mask = np.around(np.random.uniform(0., 1., size=(100, 100))).astype(np.float32)

        for mask in [None, random_mask]:
            with self.subTest(f"No iterative refinement, mask: {mask is None}"):
                estimated_stds = cmrsim_snr.compute_noise_std(image, target_snr,
                                                              coil_sensitivities=coil_sensitivities,
                                                              mask=mask, iteratively_refine=False)
                self.assertTrue(np.allclose(correct_stds, estimated_stds))

        with self.subTest("With iterative refinement"):
            estimated_stds = cmrsim_snr.compute_noise_std(image, target_snr, coil_sensitivities=coil_sensitivities,
                                                          mask=None, iteratively_refine=True)
            snr_tolerance = np.sqrt(1/n_coils) - (1 / 1.05 * np.sqrt(1/n_coils))
            self.assertTrue(np.allclose(correct_stds, estimated_stds, rtol=snr_tolerance))


class TestCalculateSNR(unittest.TestCase):
    """ Tests: cmrsim.utils.snr.calculate_snr  """

    def test_calculate_multicoil_snr(self):
        """

        :return:
        """
        for n_coils in [2, 8]:
            with self.subTest(f"Testing with {n_coils} coils"):
                image = np.ones((n_coils, 400, 400), dtype=np.complex64)
                coil_sensitivities = np.ones_like(image) / n_coils
                image_noiseless = image * coil_sensitivities

                for noise_std in np.array([0.1, 0.05, 0.01]):
                    noise_vectors = tf.random.normal(shape=(2,) + tuple(image_noiseless.shape)) * noise_std
                    complex_noise_map = tf.complex(noise_vectors[0], noise_vectors[1])
                    noisy_image = image_noiseless + complex_noise_map
                    snr_map = cmrsim_snr.calculate_snr(single_coil_images=noisy_image,
                                                       dynamic_noise_scan=complex_noise_map,
                                                       coil_sensitivities=coil_sensitivities)
                    mean_snr = tf.reduce_mean(snr_map)
                    self.assertTrue(np.allclose(mean_snr, np.sqrt(1/n_coils)/noise_std, rtol=0.2))

    def test_calculate_single_coil_snr(self):
        """

        :return:
        """
        image_noiseless = np.ones((1, 400, 400), dtype=np.complex64)
        coil_sensitivities = np.ones_like(image_noiseless)

        for noise_std in np.array([0.1, 0.05, 0.01]):
            noise_vectors = tf.random.normal(shape=(2,) + tuple(image_noiseless.shape)) * noise_std
            complex_noise_map = tf.complex(noise_vectors[0], noise_vectors[1])
            noisy_image = image_noiseless + complex_noise_map
            snr_map = cmrsim_snr.calculate_snr(single_coil_images=noisy_image,
                                               dynamic_noise_scan=complex_noise_map,
                                               coil_sensitivities=coil_sensitivities)
            mean_snr = tf.reduce_mean(snr_map)
            self.assertTrue(np.allclose(mean_snr, 1/noise_std, rtol=0.2))
