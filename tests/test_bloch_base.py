import unittest
import tensorflow as tf
tf.config.run_functions_eagerly(True)
import numpy as np

import cmrsim

from pint import Quantity

GAMMA = Quantity(267.515, "rad/mT/ms")


class TestRFPulseRotation(unittest.TestCase):

    def test_random_rotations(self):
        """Randomly rotates a vector and checks that magnitude remains 1
        and checks that always: M+ = conj(M-)
        """

        num_flips = 10
        fa = tf.random.uniform((num_flips,), minval=0, maxval=np.pi, dtype=tf.float32)
        phase = tf.random.uniform((num_flips,), minval=-np.pi, maxval=np.pi, dtype=tf.float32)
        hardpulse_mod = cmrsim.bloch.BaseBlochOperator("test")
        complex_alpha = tf.complex(fa, 0.) * tf.exp(tf.complex(0., phase))
        M = tf.constant([0.+0j, 0.+0j, 1.+0j], dtype=tf.complex64)
        M = hardpulse_mod.hard_pulse(complex_alpha, M)
        Mag = np.sqrt(abs(M[:, 0]) ** 2 + abs(M[:, 2]) ** 2)
        self.assertTrue(np.allclose(tf.abs(Mag), 1., atol=1e-6))
        self.assertTrue(np.allclose(np.real(M[:, 0]), np.real(M[:, 1]), atol=1e-6))
        self.assertTrue(np.allclose(np.imag(M[:, 0]), -np.imag(M[:, 1]), atol=1e-6))
        self.assertTrue(np.allclose(np.imag(M[:, 2]), 0., atol=1e-6))

    def test_zero_phase(self):
        complex_alpha = tf.complex(tf.constant([np.pi / 2, ], dtype=tf.float32), 0.)
        hardpulse_mod = cmrsim.bloch.BaseBlochOperator("test")
        M = tf.reshape(tf.constant([0.+0j, 0.+0j, 1.+0j], dtype=tf.complex64), [1, 3])
        M = hardpulse_mod.hard_pulse(complex_alpha, M)
        transverse_m = M[:, 0].numpy()
        self.assertTrue(np.allclose(transverse_m.real, 1, atol=1e-6))
        self.assertTrue(np.allclose(transverse_m.imag, 0, atol=1e-6))
        self.assertTrue(np.allclose(np.abs(M[:, 2]), 0, atol=1e-6))

    def test_90_phase(self):
        complex_alpha = tf.complex(0., tf.constant([np.pi / 2, ], dtype=tf.float32))
        hardpulse_mod = cmrsim.bloch.BaseBlochOperator("test")
        M = tf.reshape(tf.constant([0.+0j, 0.+0j, 1.+0j], dtype=tf.complex64), [1, 3])
        M = hardpulse_mod.hard_pulse(complex_alpha, M)
        transverse_m = M[:, 0].numpy()
        self.assertTrue(np.allclose(transverse_m.real, 0, atol=1e-6))
        self.assertTrue(np.allclose(transverse_m.imag, 1, atol=1e-6))
        self.assertTrue(np.allclose(np.abs(M[:, 2]), 0, atol=1e-6))