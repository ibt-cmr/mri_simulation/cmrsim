import unittest

from cmrsim.simulation_templates.experimental_optimization import SimulationOptimizer
import tensorflow as tf
tf.config.run_functions_eagerly(False)

from cmrsim.analytic import CompositeSignalModel

import cmrsim
import tests.phantoms


class DiffDummySim(cmrsim.analytic.AnalyticSimulation):
    def build(self) -> ('CompositeSignalModel', 'BaseSampling', 'BaseRecon'):
        # Encoding definition
        encoding_module = cmrsim.analytic.encoding.EPI(field_of_view=(0.31, 0.112),
                                                       sampling_matrix_size=(60, 22),
                                                       bandwidth_per_pixel=1300., blip_duration=0.,
                                                       k_space_segments=22,
                                                       acquisition_start=-25.7692,
                                                       absolute_noise_std=0.)

        # Signal module construction
        spin_echo_module = cmrsim.analytic.contrast.SpinEcho(tf.constant([10., ]),
                                                             tf.constant([5000., ]))

        # Forward model composition
        forward_model = CompositeSignalModel(spin_echo_module)

        # Reconstruct module
        recon_module = cmrsim.reconstruction.Cartesian2D(sample_matrix_size=(60, 22))

        return forward_model, encoding_module, recon_module


@unittest.skip("Tf function must be diabled for coverage but the gradient tape requires it to work,"
               "which causes an unsolvable issue in testing right now")
class TestDifferentiability(unittest.TestCase):
    def setUp(self) -> None:
        x, y = 121, 43
        fov = (0.3024, 0.1072)
        phantom_dict, phantom_ref, coords = tests.phantoms.define_shepplogan((0.3024, 0.1072),
                                                                             (121, 43))
        self.dataset = tests.phantoms.dict_to_base(phantom_dict)

    def run_optimization_loop(self, exp_optimizer, variables_to_watch, n=10):

        for step in range(5):
            img, loss, grads = exp_optimizer.optimization_step(self.dataset(batchsize=1000),
                                                               variables_to_watch,
                                                               vars_to_watch_recon=[],
                                                               points_in_dataset=self.dataset.set_size)

    @unittest.skip("Shape handling for multiple repetitions is broken")
    def test_multiple_repetitions(self):
        simulator = DiffDummySim(build_kwargs=dict())
        simulator.forward_model.spin_echo.TE = tf.Variable([10., 20., 30.], shape=(3,),
                                                           dtype=tf.float32, name='TE')
        simulator.forward_model.spin_echo.TR = tf.Variable([1000., 2000., 3000.], shape=(3,),
                                                           dtype=tf.float32, name='TR')
        simulator.forward_model.update()

        optimizer = tf.keras.optimizers.Adam(lr=1.)
        exp_optimizer = SimulationOptimizer(simulator, optimizer)
        variables_to_watch = simulator.forward_model.variables[0:1]

        initial_value = variables_to_watch[0].read_value().numpy()
        self.run_optimization_loop(exp_optimizer, variables_to_watch, n=2)
        end_value = variables_to_watch[0].read_value().numpy()
        self.assertTrue(all((v1 < v2 for v1, v2 in zip(initial_value, end_value))),
                        msg="TE did not increase during optimization")

    def test_single_repetition(self):
        simulator = DiffDummySim(build_kwargs=dict())
        simulator.forward_model.spin_echo.TE = tf.Variable([10., ], shape=(1,),
                                                           dtype=tf.float32, name='TE')
        simulator.forward_model.spin_echo.TR = tf.Variable([1000., ], shape=(1,),
                                                           dtype=tf.float32, name='TR')
        simulator.forward_model.update()

        optimizer = tf.keras.optimizers.Adam(lr=0.5)
        exp_optimizer = SimulationOptimizer(simulator, optimizer)
        variables_to_watch = simulator.forward_model.variables[0:1]

        initial_value = variables_to_watch[0].read_value().numpy()
        self.run_optimization_loop(exp_optimizer, variables_to_watch, n=2)
        end_value = variables_to_watch[0].read_value().numpy()
        self.assertLess(initial_value, end_value, msg="TE did not increase during optimization")
