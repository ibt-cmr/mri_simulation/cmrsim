import tensorflow as tf

from cmrsim.reconstruction import Cartesian2DMultiCoil

if __name__ == "__main__":
    rmrecon = Cartesian2DMultiCoil((10, 10),
                                   coil_sensitivities=tf.cast(tf.random.normal((4, 10, 10)),
                                                              tf.complex64))
    a = rmrecon(tf.cast(tf.random.normal((2, 4, 2, 10 * 10)), tf.complex64), coil_channel_axis=2)
