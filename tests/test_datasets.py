import os

import pyvista
import tests.phantoms

import unittest
import collections

import numpy as np
from pint import Quantity

import cmrsim.utils.particle_properties as part_factory
import cmrsim.datasets


test_output_dir = f"{os.path.dirname(__file__)}/test_output/datasets"
os.makedirs(test_output_dir, exist_ok=True)


class TestBaseDataset(unittest.TestCase):
    def setUp(self) -> None:
        x, y, = 100, 100
        valid_data = dict(M0=np.around(np.random.uniform(0., 1., size=(x * y, 1, 1)), decimals=0),
                          r_vectors=np.ones((x * y, 1, 1, 3), dtype=np.float32),
                          T1=np.ones((x * y, 1, 1)),
                          T2=np.ones((x * y, 1, 1)))
        self.valid_data = collections.OrderedDict({k: v.astype(np.float32)
                                                   for k, v in valid_data.items()})
        self.valid_data["M0"] = (self.valid_data["M0"] + 0.*1j).astype(np.complex64)

    def test_valid_shape(self):
        """ Constructs dataset from valid input data and tests if the keys,
        type and batch-shape is correct when iterating over the dataset."""
        data_set_module = cmrsim.datasets.AnalyticDataset(self.valid_data)
        set_size = data_set_module.set_size
        batchsize = 1000
        remainder = set_size % batchsize
        for idx, batch, in data_set_module(batchsize=batchsize).enumerate():
            self.assertListEqual(list(self.valid_data.keys()), list(batch.keys()),
                                 msg="Keys in voxel batch is unequal to data keys")
            self.assertTrue(all([(v.shape[0] == 1000 or v.shape[0] == remainder)
                                 for v in batch.values()]),
                            msg="Batch shape does not match expected batch size")

    def test_invalid_shape(self):
        """ Tries to construct a dataset from invalid data
         (without the unitary dimensions via np.squeeze) and asserts that a ValueError is raised"""
        invalid_data = collections.OrderedDict([(k, np.squeeze(v))
                                                for k, v in self.valid_data.items()])
        self.assertRaises(ValueError, lambda: cmrsim.datasets.AnalyticDataset(invalid_data))
        cmrsim.datasets.AnalyticDataset(invalid_data, expand_dimension=True)


class TestBlochDataset(unittest.TestCase):
    def setUp(self) -> None:
        n_particles = 10000
        n_timesteps = 500
        valid_data = collections.OrderedDict(
                        magnetization=np.tile(np.array([[0., 0., 1]]), [n_particles, 1]),
                        trajectories=np.random.normal(0., 1., size=(n_particles, n_timesteps, 3)),
                        T1=np.random.uniform(1000, 1200, size=(n_particles, )),
                        T2=np.random.uniform(300, 600, size=(n_particles, )),
                        M0=np.random.uniform(10, 15, size=(n_particles, ))
                        )

        self.dataset = cmrsim.datasets.BlochDataset(valid_data,
                                                    filter_inputs=True)

    def test_valid_shape(self):
        ds = self.dataset(batchsize=1000)
        for idx, batch in ds.take(1).enumerate():
            print(f"Iteration: {idx}", end="\n\t")
            print("\n\t".join([f"{k}: {v.shape}" for (k, v) in batch.items()]))

    def test_phantoms(self):
        phantom_dict, _, _ = tests.phantoms.define_cylinder_phantom((0.3, 0.3), (150, 125))
        dataset = tests.phantoms.dict_to_bloch(phantom_dict, n_time_steps=1000)
        for idx, batch in dataset(1000, 5).take(1).enumerate():
            print(f"Iteration: {idx}", end="\n\t")
            print("\n\t".join([f"{k}: {v.shape}" for (k, v) in batch.items()]))


class TestFlowDataset(unittest.TestCase):
    def setUp(self) -> None:
        import pyvista as pv
        import os
        mesh = pv.read(f"{os.path.dirname(os.path.abspath(__file__))}"
                       f"/resources/CM_PD/steady_stenosis.vtk")
        mesh = mesh.cell_data_to_point_data()

        pixel_spacing = np.array((0.5e-3, 0.5e-3, 0.5e-3))
        slice_normal = np.array([1., 0., 0.])
        slice_position = np.array([0.25, 0., 0.])
        slice_thickness = 2.e-2

        particle_creation_fncs = {"M0": part_factory.norm_magnetization(),
                                  "T1": part_factory.uniform(default_value=1300.),
                                  "T2": part_factory.uniform(default_value=300.)}

        self.dataset = cmrsim.datasets.RefillingFlowDataset(
                                                mesh,
                                                particle_creation_callables=particle_creation_fncs,
                                                slice_position=slice_position,
                                                slice_normal=slice_normal,
                                                slice_thickness=slice_thickness,
                                                seeding_vol_spacing=pixel_spacing,
                                                lookup_map_spacing=pixel_spacing,
                                                field_list=[("U", 3), ])

        velocity_field_3d, map_dimensions = self.dataset.get_lookup_table()
        self.trajectory_module = cmrsim.trajectory.FlowTrajectory(velocity_field_3d,
                                                                  map_dimensions=map_dimensions,
                                                                  device="GPU:0")

    def test_call(self):
        with self.subTest("Initial call"):
            pos, props, _ = self.dataset(particle_density=8)
        with self.subTest("repeated_call"):
            # With reseed threshold=0 and at least one particle in each mesh cell there should not
            # be additional particles after calling
            pos1, props1, _ = self.dataset(particle_density=8, residual_particle_pos=pos,
                                           particle_properties=props, distance_tolerance=1e-3,
                                           reseed_threshold=0)
            self.assertTrue(np.allclose(pos, pos1))

    def test_initial_filling(self):
        pos, props = self.dataset.initial_filling(particle_density=8)
        new_mesh = self.dataset.project_density_to_mesh(pos)
        self.assertTrue(np.mean(new_mesh["density"]) > 6)
        self.assertTrue(np.mean(new_mesh["density"]) <= 8)


class TestMeshDatasets(unittest.TestCase):
    def test_loading(self):
        file_path = os.path.dirname(__file__)

        files = [f'{file_path}/../notebooks/example_resources/mesh_displacements/Displ{i:03}.vtk'
                 for i in range(3)]
        with self.subTest("from list of files"):
            timing = Quantity(np.loadtxt(f"{file_path}/../notebooks/example_resources/"
                                         "mesh_displacements/PV_loop_reordered.txt")[:3, 1], "ms")
            mesh_dataset = cmrsim.datasets.CardiacMeshDataset.from_list_of_meshes(
                                    files, timing, time_precision_ms=3, mesh_numbers=(4, 40, 30))

            mesh_dataset.mesh.save(f"{file_path}/test_output/datasets/mesh.vtk")

        with self.subTest("non existing file"):
            self.assertRaises(FileNotFoundError,
                              lambda: cmrsim.datasets.CardiacMeshDataset.from_list_of_meshes(
                                    ["adasffdafsafa", "asfaf"], timing, time_precision_ms=3,
                                       mesh_numbers=(4, 40, 30)))

        with self.subTest("from single file"):
            mesh_dataset = cmrsim.datasets.CardiacMeshDataset.from_single_vtk(
                f"{file_path}/test_output/datasets/mesh.vtk", mesh_numbers=(4, 40, 30))

        with self.subTest("from arrays"):
            initial_mesh = pyvista.read(files[0])
            arrays = [np.array(pyvista.read(f).points) for f in files[1:]]
            for i, a in enumerate(arrays):
                np.save(f"{file_path}/test_output/datasets/arr{i:02}", a)
            list_of_arrays = [f"{file_path}/test_output/datasets/arr{i:02}.npy"
                              for i in range(len(arrays))]
            mesh_dataset = cmrsim.datasets.CardiacMeshDataset.from_list_of_arrays(
                                initial_mesh, (4, 40, 30), list_of_files=list_of_arrays,
                                timing=Quantity(mesh_dataset.timing, "ms"))

        with self.subTest("non existing file"):
            self.assertRaises(FileNotFoundError,
                              lambda: cmrsim.datasets.CardiacMeshDataset.from_list_of_arrays(
                                initial_mesh, (4, 40, 30), list_of_files=["asdsd", "adasa"],
                                timing=Quantity(mesh_dataset.timing, "ms")))

    def _load_mesh(self):
        file_path = os.path.dirname(__file__)
        files = [f'{file_path}/../notebooks/example_resources/mesh_displacements/Displ{i:03}.vtk'
                 for i in range(3)]
        timing = Quantity(np.loadtxt(f"{file_path}/../notebooks/example_resources/"
                                     "mesh_displacements/PV_loop_reordered.txt")[:3, 1], "ms")
        mesh_dataset = cmrsim.datasets.CardiacMeshDataset.from_list_of_meshes(
            files, timing, time_precision_ms=3, mesh_numbers=(4, 40, 30))
        return mesh_dataset

    def test_base_funcs(self):
        mesh_dataset = self._load_mesh()
        mesh_dataset.get_field("displacements", [0, 1])
        r, t = mesh_dataset.get_trajectories(start=Quantity(0., "ms"),
                                             end=Quantity(mesh_dataset.timing[-1], "ms"))
        r = mesh_dataset.get_positions_at_time(Quantity(0., "ms"))

        mesh_dataset.mesh["ref_vec"] = np.random.normal(0., 1., size=mesh_dataset.mesh.points.shape)
        mesh_dataset.transform_vectors(mesh_dataset.timing[1:2], ["ref_vec"])

        slice_mesh = mesh_dataset.select_slice(slice_position=Quantity([0., 0., -3], "cm"),
                                               slice_normal=np.array([0., 0., 1.]),
                                               slice_thickness=Quantity(10, "mm"),
                                               reference_time=Quantity(0, "ms"))
        slice_mesh = cmrsim.datasets.MeshDataset(slice_mesh, timing_ms=mesh_dataset.timing)
        npoints = slice_mesh.mesh.points.shape[0]
        slice_mesh.add_data_per_time(np.random.normal(0., 1., size=(3, npoints)),
                                     name="random_scalar")
        slice_mesh.add_data_per_time(np.random.normal(0., 1., size=(3, npoints, 3)),
                                     name="random_vector")

        from sys import platform
        if platform == "linux" or platform == "linux2":
            pyvista.start_xvfb()
        slice_mesh.render_input_animation(f"{test_output_dir}/mesh_render",
                                          scalar="random_scalar", vector="random_vector")

    def test_meshutils(self):
        mesh_dataset = self._load_mesh()
        refined_mesh = mesh_dataset.refine(2, 2, 2)

        connectivity = mesh_dataset.evaluate_connectivity(40, 4, 30)
        cell_sizes = mesh_dataset.evaluate_cellsize(mesh_dataset.mesh.points,
                                                    mesh_dataset.mesh.cell_connectivity.reshape(-1, 4))
        mesh_dataset.evaluate_cylinder_coordinates(mesh_dataset.mesh.points, 4, 40*30+1)
        mesh_dataset.compute_local_basis(time_stamp=Quantity(0., "ms"))
        mesh_dataset.compute_cylinder_coords(time_stamp=Quantity(0., "ms"))


class TestRegularGridDataset(unittest.TestCase):
    def test_shepp_logan(self):
        dataset = cmrsim.datasets.RegularGridDataset.from_shepp_logan(
            map_size=(100, 100, 5), extent=Quantity([10, 10, 5], "cm"))

    def test_from_unstructured_grid(self):
        sphere = pyvista.Sphere(radius=0.1, center=(0., 0., 0.))
        ds = cmrsim.datasets.RegularGridDataset.from_unstructured_grid(
            sphere, pixel_spacing=Quantity([4., 4., 4.], "mm"),
            padding=Quantity([1., 1., 1.], "cm"))

    def test_general_functionality(self):
        sphere = pyvista.Sphere(radius=0.1, center=(0., 0., 0.))
        ds = cmrsim.datasets.RegularGridDataset.from_unstructured_grid(
            sphere, pixel_spacing=Quantity([4., 4., 4.], "mm"),
            padding=Quantity([1., 1., 1.], "cm"))

        ds.mesh["M0"] = np.where(ds.mesh["vtkValidPointMask"],
                                  np.ones_like(ds.mesh["vtkValidPointMask"]),
                                  np.zeros_like(ds.mesh["vtkValidPointMask"]))

        with self.subTest("compute_offres"):
            # susceptibilty in ppm
            ds.mesh["chi"] = np.where(ds.mesh["vtkValidPointMask"],
                                      np.ones_like(ds.mesh["vtkValidPointMask"]) * (-9.05),
                                      np.ones_like(ds.mesh["vtkValidPointMask"]) * 0.36) * 1e-6
            ds.compute_offresonance(b0=Quantity(3, "T"), susceptibility_key="chi")

        with self.subTest("select_slice"):
            slice_normal = np.array([0., 1., 1.])
            readout_direction = np.array([1., 1., 0.])
            slice_position = Quantity([0., 0., 1], "cm")
            field_of_view = Quantity([30, 30, 1], "cm")
            spacing = Quantity([1, 1, 1], "mm")

            global_slice = ds.select_slice(slice_normal, spacing, slice_position,
                                           field_of_view, readout_direction, in_mps=False)
            mps_slice = ds.select_slice(slice_normal, spacing, slice_position, field_of_view,
                                        readout_direction, in_mps=True)
            global_slice = ds.select_slice(slice_normal, spacing, slice_position,
                                           field_of_view, in_mps=False)
            self.assertRaises(ValueError, lambda: ds.select_slice(slice_normal, spacing,
                                        slice_position, field_of_view, slice_normal, in_mps=False))

        with self.subTest("get_phantom_dict"):
            ds.mesh["is_non_trivial"] = ds.mesh["M0"] > 0.
            phantom_dict = ds.get_phantom_def(filter_by="is_non_trivial", keys=["M0", "offres"],
                                              perturb_positions_std=Quantity(0.1, "mm").m_as("m"))

        with self.subTest("add_field"):
            dat = np.ones(ds.mesh.dimensions)
            ds.add_field(key="dummy", data=dat)
            self.assertTrue("dummy" in ds.mesh.array_names)

        with self.subTest("resample_spacing"):
            ds.resample_spacing(spacing=Quantity([3, 3, 3], "mm"))

