import unittest
import os
from typing import List

import tensorflow as tf
import numpy as np
from pint import Quantity
import matplotlib.pyplot as plt

import cmrsim.trajectory
from cmrsim.trajectory import FlowTrajectory, TimeVaryingVelocityField, TaylorTrajectoryN

tf.config.run_functions_eagerly(True)

output_dir = os.path.abspath(f"{os.path.dirname(__file__)}/test_output/trajectory/")
os.makedirs(output_dir, exist_ok=True)
plt.close("all")


class TestInvariants(unittest.TestCase):
    """Tests if the signatures and invariants of the bound methods \_\_call\_\_ as well as
    increment particles are fulfilled for all available trajectory modules"""

    traj_modules: List[cmrsim.trajectory.BaseTrajectoryModule]

    def setUp(self) -> None:
        plt.close("all")
        """Creates a dummy instance of all available trajectory modules"""
        # Static
        self.traj_modules = [cmrsim.trajectory.StaticParticlesTrajectory()]

        # Laminar flow
        constant_velocities = Quantity(np.zeros([21, 21, 200, 3], dtype=np.float32), "m/s")
        constant_velocities[..., 2] = Quantity(10, "cm/s")
        map_dimension = Quantity([[-4, 4], [-4, 4], [-10, 30]], "cm").to("m")
        _flow_mod = FlowTrajectory(np.array(constant_velocities.m_as("m/ms"), dtype=np.float32),
                                   np.array(map_dimension.m_as("m"), dtype=np.float32), [])
        self.traj_modules.append(_flow_mod)

        # Time-variant flow
        lin_inc_velocities = constant_velocities.m_as("cm/s")[np.newaxis]
        lin_inc_velocities = Quantity(lin_inc_velocities *
                                      np.linspace(0, 1, 11).reshape(11, 1, 1, 1, 1), "cm/s")
        time_grid = Quantity(np.arange(0, 51, 5), "ms")
        _time_var_flow_mod = TimeVaryingVelocityField(time_grid.m_as("ms"),
                                    np.array(lin_inc_velocities.m_as("m/ms"), dtype=np.float32),
                                    np.array(map_dimension.m_as("m"), dtype=np.float32), [])
        self.traj_modules.append(_time_var_flow_mod)

        # Turbulent flow
        additional_fields = [("Reflectionmap", 1), ]
        cholesky = np.random.uniform(0, 1, size=(*constant_velocities.shape[0:-1], 6))
        langevin_frequency = np.random.uniform(0, 1, size=(*constant_velocities.shape[0:-1], 1))
        reflection_map = np.random.uniform(0, 1, size=(*constant_velocities.shape[0:-1], 1))
        turb_lum = np.concatenate([constant_velocities.m, cholesky, langevin_frequency,
                                   reflection_map], axis=-1).astype(np.float32)
        _turbulent_flow_mod = cmrsim.trajectory.TurbulentTrajectory(
                                turb_lum, map_dimension.m_as("m").astype(np.float32),
                                additional_dimension_mapping=additional_fields)
        self.traj_modules.append(_turbulent_flow_mod)

        # TalyorN
        r0 = np.random.uniform(-0.5, 0.5, size=(10, 3)).reshape(10, 1, 3)
        v0 = np.random.normal(-0.5, 0.5, size=(10, 3)).reshape(10, 1, 3)
        time = np.linspace(0, 1, 20)
        r_of_t = v0 * time.reshape(1, 20, 1) + r0
        _taylor_mod = cmrsim.trajectory.TaylorTrajectoryN(order=3, time_grid=time,
                                                          particle_trajectories=r_of_t)
        self.traj_modules.append(_taylor_mod)

        # POD
        vecs = np.repeat(np.random.uniform(1, 2, size=(10, 1, 3)), time.shape[0], axis=1)
        vecs /= np.linalg.norm(vecs, axis=-1, keepdims=True)
        _pod_module = cmrsim.trajectory.PODTrajectoryModule(time, trajectories=r_of_t,
                                                            n_modes=2, poly_order=2,
                                                            additional_data=dict(velocity=vecs))
        self.traj_modules.append(_pod_module)

        # Diffusion + bulk
        loc_basis = np.tile(np.eye(3, 3).reshape(1, 1, 9), [len(r0), len(time), 1])
        diffusivities = Quantity(np.repeat(np.array([2, 1.25, 0.5]).reshape(1, 3), len(r0), axis=0),
                                 "mm**2/s")
        _diff_mod = cmrsim.trajectory.DiffusionTaylor(order=4, time_grid=Quantity(time, "ms"),
                                                      particle_trajectories=Quantity(r_of_t, "m"),
                                                      local_basis_over_time=loc_basis,
                                                      diffusivity=diffusivities,
                                                      particles_per_node=10)
        self.traj_modules.append(_diff_mod)

        # Breathing
        _breathing = cmrsim.trajectory.SimpleBreathingMotionModule.from_sinosoidal_motion(
                        cmrsim.trajectory.StaticParticlesTrajectory(),
                        breathing_period=Quantity(2.300, "s"),
                        breathing_direction=np.array([0.5, 2., 1.]),
                        breathing_amplitude=Quantity(4.5, "cm"))
        self.traj_modules.append(_breathing)

    def test_run_particle_increment(self):
        r_0 = np.zeros([1, 3], dtype=np.float32)
        for traj_mod in self.traj_modules:
            with self.subTest(f"Increment particles [{traj_mod.__class__}]"):
                r_new, addfields = traj_mod.increment_particles(particle_positions=tf.constant(r_0),
                                                                dt=tf.constant(0.1))

    def test_run_call(self):
        new_t = np.array([0., 0.1], dtype=np.float32)
        r_0 = np.zeros([1, 3], dtype=np.float32)
        for traj_mod in self.traj_modules:
            with self.subTest(f"Call function [{traj_mod.__class__}]"):
                r_new, _ = traj_mod(initial_positions=tf.constant(r_0), timing=tf.constant(new_t),
                                    dt_max=0.05)


class TestFlowTrajectory(unittest.TestCase):
    def setUp(self) -> None:
        plt.close("all")
        constant_velocities = Quantity(np.zeros([21, 21, 200, 3], dtype=np.float32), "m/s")
        constant_velocities[..., 2] = Quantity(10, "cm/s")
        map_dimension = Quantity([[-4, 4], [-4, 4], [-10, 30]], "cm").to("m")
        field_list = []
        self.trajectory_module = FlowTrajectory(
                                    np.array(constant_velocities.m_as("m/ms"), dtype=np.float32),
                                    np.array(map_dimension.m_as("m"), dtype=np.float32), field_list)

    def test_input_shapes(self):
        with self.subTest(msg="Batch of particle valid shape, increment"):
            r_init = tf.random.uniform(shape=[12, 3], minval=-0.04, maxval=0.04)
            r_new, _ = self.trajectory_module.increment_particles(r_init,
                                                                  dt=tf.constant(0.01, tf.float32))
            ref = np.array([[0., 0., 1e-6]])
            delta = np.abs(np.abs(r_init - r_new) - ref)
            self.assertTrue(np.allclose(delta, 0., atol=5e-9), msg=f"{delta}")

        with self.subTest(msg="Batch of particle valid shape, __call__"):
            r_init = tf.random.uniform(shape=[1, 3], minval=-0.001, maxval=0.001)
            timing = np.linspace(0., 0.1, 10)
            r_new, fields = self.trajectory_module(r_init, timing, dt_max=0.001,
                                                   return_velocities=True)

            ref = (Quantity([0, 0, 10], "cm/s") * Quantity(0.1, "ms")).to("m").m
            delta = np.abs(np.abs(r_init[:, np.newaxis] - r_new[:]) - ref[np.newaxis, np.newaxis])
            f, a = plt.subplots(2, 1)
            a[0].plot(timing, r_new[0])
            a[1].plot(timing, delta[0])
            f.suptitle("Constant velocity field")
            f.savefig(f"{output_dir}/constant_flow_call.png")
            plt.close(f)


            self.assertTrue(np.allclose(delta[0, -1], 0., atol=2e-6),  msg=f"{delta[0, -1]}")
            self.assertTrue(np.allclose(np.stack([f["velocity"][:, 2] for f in fields]),
                                        float(Quantity(10, "cm/s").m_as("m/ms")),
                                        atol=1e-10))


class TestTimeVaryingVelocityField(unittest.TestCase):
    def setUp(self) -> None:
        plt.close("all")
        const_velocities = Quantity(np.zeros([21, 21, 200, 3], dtype=np.float32), "m/s")
        const_velocities[..., 2] = Quantity(10, "cm/s")
        lin_inc_velocities = np.tile(const_velocities.m_as("cm/s")[np.newaxis],
                                     [11, 1, 1, 1, 1])
        lin_inc_velocities = Quantity(lin_inc_velocities *
                                      np.linspace(0, 1, 11).reshape(11, 1, 1, 1, 1), "cm/s")
        time_grid = Quantity(np.arange(0, 51, 5), "ms")
        map_dimension = Quantity([[-4, 4], [-4, 4], [-10, 30]], "cm").to("m")
        field_list = []
        self.trajectory_module = TimeVaryingVelocityField(
                                    time_grid.m_as("ms"),
                                    np.array(lin_inc_velocities.m_as("m/ms"), dtype=np.float32),
                                    np.array(map_dimension.m_as("m"), dtype=np.float32), field_list)

    def test_input_shapes(self):
        with self.subTest(msg="Batch of particle valid shape, increment"):
            r_init = tf.random.uniform(shape=[1, 3], minval=-0.04, maxval=0.04)
            r_new, _ = self.trajectory_module.increment_particles(r_init,
                                                                  dt=tf.constant(0.1, tf.float32))
            r_new, _ = self.trajectory_module.increment_particles(r_new,
                                                                  dt=tf.constant(0.1, tf.float32))
            delta = np.abs(r_init - r_new)

    def test_parabolic_position(self):
        self.trajectory_module.reset_time()
        r_init = tf.random.uniform(shape=[1, 3], minval=-0.001, maxval=0.001)
        timing = np.arange(0.0, 1.005, 0.05)
        trajectories, fields = self.trajectory_module(r_init, timing, dt_max=0.01,
                                                      return_velocities=True)

        f, a = plt.subplots(1, 2)
        a[0].plot(timing, trajectories[0, ..., :])
        a[1].plot(timing, np.stack([f["velocity"][:, 2] for f in fields], axis=0))
        f.suptitle("Time-varying velocity field, constant acceleration")
        f.savefig(f"{output_dir}/time_variant_velocity_field.png")
        plt.close(f)


class TestTaylorTrajectoryN(unittest.TestCase):
    """ Checks if a simple slightly noise quadratic trajectory is fitted and evlauated correclty"""
    def setUp(self) -> None:
        plt.close("all")
        self.ref_timing = Quantity(np.arange(0., 2.01, 0.04), "ms")
        self.fine_timing = Quantity(np.linspace(0., 2., 200), "ms")
        # acceleration = Quantity([9.81, -9.81, 9.81/2], "m/s^2").to("mm/ms^2")
        self.velocity = Quantity([20, -20, 10], "cm/s").to("mm/ms")
        self.ref_trajectory = self.ref_timing[np.newaxis, :, np.newaxis] * self.velocity[np.newaxis, np.newaxis, :]
        self.noisy_trajectory = (self.ref_trajectory +
                                 Quantity(np.random.normal(0., Quantity(0.01, "mm").m,
                                                           size=self.ref_trajectory.shape), "mm"))

    def _plot_trajectory(self, fine_trajectory):
        f, a = plt.subplots(1, 1)
        fit_arts = a.plot(self.fine_timing.m_as("ms"), Quantity(fine_trajectory[0]),
                          label=["Fit x", "Fit y", "Fit z"])
        a.set_prop_cycle(None)
        ref_arts1 = a.plot(self.ref_timing.m_as("ms"), self.noisy_trajectory[0].m_as("m"),
                           marker="x", linewidth=0,
                           label=["Ref Noisy x", "Ref Noisy y", "Ref Noisy z"])
        a.set_prop_cycle(None)
        ref_arts2 = a.plot(self.ref_timing.m_as("ms"), self.ref_trajectory[0].m_as("m"),
                           linestyle="--", label=["Ref x", "Ref y", "Ref z"])
        a.set_title("linear noisy trajectory fit")
        a.set_ylabel("position in meter")
        a.set_xlabel("time in ms")
        a.legend(ncol=3)
        return f

    def test_fit_and_eval(self):
        module = TaylorTrajectoryN(order=3, time_grid=self.ref_timing.m_as("ms"),
                                   particle_trajectories=self.ref_trajectory.m_as("m"),
                                   fit_on_init=True)

        fine_trajectory, _ = module(initial_positions=None,
                                    timing=tf.constant(self.fine_timing.m_as("ms"), dtype=tf.float32))
        self.assertTrue(np.allclose(module.optimal_parameters.read_value().numpy()[0, 1, :],
                                    self.velocity.m_as("m/ms"), atol=1e-8))
        f = self._plot_trajectory(fine_trajectory)
        f.savefig(f"{output_dir}/test_taylor_trajectory_call.svg")
        plt.close(f)

    def test_increment_particles(self):
        tiled_trajectories = Quantity(np.repeat(self.ref_trajectory.m_as("m"),
                                                10, axis=0), "m")
        module = TaylorTrajectoryN(order=6, time_grid=self.ref_timing.m_as("ms"),
                                   particle_trajectories=tiled_trajectories.m_as("m"))
        fine_trajectory = [module(initial_positions=None,
                                  timing=tf.constant(self.fine_timing[0].m_as("ms"),
                                              dtype=tf.float32))[0][:, 0]]
        for dt in np.diff(self.fine_timing.m_as("ms")):
            fine_trajectory.append(module.increment_particles(None, dt)[0])
        fine_trajectory = np.stack(fine_trajectory, axis=1)

        self.assertTrue(np.allclose(module.optimal_parameters.read_value().numpy()[0, 1, :],
                                    self.velocity.m_as("m/ms"), atol=1e-8))

        f = self._plot_trajectory(fine_trajectory)
        f.savefig(f"{output_dir}/test_taylor_trajectory_increment.svg")
        plt.close(f)


class TestBreathingMotion(unittest.TestCase):

    def test_wrap_static(self):
        trajectory_breathing = cmrsim.trajectory.SimpleBreathingMotionModule.from_sinosoidal_motion(
            cmrsim.trajectory.StaticParticlesTrajectory(),
            breathing_period=Quantity(2.300, "s"),
            breathing_direction=np.array([0.5, 2., 1.]),
            breathing_amplitude=Quantity(4.5, "cm"))
        timing = Quantity(np.linspace(0, 2.3, 100).astype(np.float32), "s")  # Used for the __call__ method
        dt = tf.constant((timing[1] - timing[0]).m_as("ms"), dtype=tf.float32)

        r_0 = np.array([[0., 0., 0.]], dtype=np.float32)
        r_breath, _ = trajectory_breathing(initial_positions=r_0, timing=timing.m_as("ms"))
        r_breath_inc = np.swapaxes(
            np.stack([trajectory_breathing.increment_particles(particle_positions=r_0, dt=dt)[0]
                      for _ in timing]), 0, 1)

