import unittest

import tensorflow as tf
tf.config.run_functions_eagerly(True)

import time
import numpy as np

from cmrsim.analytic.contrast import LookUpTableModule


class TestLookupModule(unittest.TestCase):
    def setUp(self) -> None:
        """ Sets up dummy module with multiple repetitions and map channels """
        # Set up lookup table with shape (channels, #Xbins, #Ybins, #Zbins)
        # spanning the physical dimensions (X, Y, Z) mm
        self.lut_shape = (xbin, ybin, zbin, n_channels) = (151, 151, 11, 5)
        self.dimensions = np.array([(-75., 75.), (-75, 75.), (-5, 5.)], dtype=np.float32)

        lookup = np.linspace(0., 1., xbin * ybin * zbin, dtype=np.float32).reshape([xbin, ybin, zbin])
        lookup = lookup[..., np.newaxis] * np.arange(0, n_channels).reshape([1, 1, 1, n_channels])
        assert all([s1 == s2 for s1, s2 in zip(lookup.shape, self.lut_shape)])
        self.look_up_module = LookUpTableModule(look_up_map3d=lookup.astype(np.float32),
                                                map_dimensions=self.dimensions)

    def test_valid_lookup(self):
        """ As described in the docstring test random lookup input with correct shape that lies
         within the range (-dims/2, dims/2). Subtest with multiple repetitions and
         sampling points are performed. This test just expects not to throw any errors
        """
        for voxels in [1, 100]:
            with self.subTest(msg=f"Look-up with {voxels} points"):
                pseudo_rvecs = tf.random.uniform((voxels, 3), minval=0., maxval=1.,
                                                 dtype=tf.float32)
                scaling = tf.constant(np.array(self.dimensions[:, 0] - self.dimensions[:, 1]),
                                      dtype=tf.float32)
                pseudo_rvecs = (pseudo_rvecs - 1/2) * scaling
                _ = self.look_up_module(r_vectors=pseudo_rvecs, method="nearest")
                _ = self.look_up_module(r_vectors=pseudo_rvecs, method="trilinear")

    def test_invalid_lookup(self):
        """ Test whether a position out of range of the look up table causes the module to fail. """

        pseudo_rvecs_too_large = tf.constant(self.dimensions)[tf.newaxis, :, 1]*10.
        self.look_up_module(r_vectors=pseudo_rvecs_too_large)

        pseudo_rvecs_negative_too_large = -tf.constant(self.dimensions)[tf.newaxis, :, 1]*10.
        self.look_up_module(r_vectors=pseudo_rvecs_negative_too_large)

    def test_speed(self):
        """ Repeating looks up values and reports duration """
        rvec_shape = (1000, 3)
        pseudo_rvecs = tf.random.uniform(rvec_shape, minval=0., maxval=1., dtype=tf.float32)
        scaling = tf.constant(np.array(self.dimensions[:, 0] - self.dimensions[:, 1]),
                              dtype=tf.float32)
        pseudo_rvecs = (pseudo_rvecs - 1 / 2) * scaling

        start = time.perf_counter()
        for idx in range(10):
            loop_start = time.perf_counter()
            _ = self.look_up_module(r_vectors=pseudo_rvecs)
            print(f"\rIteration index: {idx}, "
                  f"iteration duration: {time.perf_counter() - loop_start}", end='')
        print(f"\nTotal duration: {time.perf_counter() - start}, "
              f"Total number of lookups: {int(tf.reduce_prod(pseudo_rvecs.shape) / 3)}")
