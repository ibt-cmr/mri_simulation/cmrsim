import unittest

import numpy as np
import tensorflow as tf
tf.config.run_functions_eagerly(True)

import cmrsim


class TestInstaneousPulse(unittest.TestCase):
    def test_call(self):
        magnetization = tf.constant([[0., 0., 1.]], dtype=tf.complex64)
        traj_mod = cmrsim.trajectory.StaticParticlesTrajectory()
        init_pos = tf.random.normal(mean=0, stddev=0.001, shape=(1, 3)) * 0.

        module = cmrsim.bloch.InstantaneousPulse(
                        complex_flip_angle=tf.constant([np.pi/2, ], dtype=tf.complex64),
                        slice_position=tf.constant([0., 0., 0.], dtype=tf.float32),
                        slice_thickness=tf.constant(0.1, dtype=tf.float32),
                        slice_normal=tf.constant([0., 0., 1.], tf.float32),
                        slice_profile="Sinc1"
        )

        _ = module(traj_mod, init_pos, magnetization)
        self.assertTrue(np.allclose(np.abs(_)[0, 0:2], 1.))

        module = cmrsim.bloch.InstantaneousPulse(
                        complex_flip_angle=tf.constant([np.pi/2, ], dtype=tf.complex64),
                        slice_position=tf.constant([0., 0., 0.], dtype=tf.float32),
                        slice_thickness=tf.constant(0.1, dtype=tf.float32),
                        slice_normal=tf.constant([0., 0., 1.], tf.float32),
                        slice_profile=None
        )
        _ = module(traj_mod, init_pos, magnetization)
        self.assertTrue(np.allclose(np.abs(_)[0, 0:2], 1.))


class TestPerfectSpoiling(unittest.TestCase):
    def test_call(self):
        magnetization = tf.constant([[0., 0., 1.]], dtype=tf.complex64)
        traj_mod = cmrsim.trajectory.StaticParticlesTrajectory()
        init_pos = tf.random.normal(mean=0, stddev=0.001, shape=(1, 3)) * 0.

        module = cmrsim.bloch.InstantaneousPulse(
                        complex_flip_angle=tf.constant([np.pi/2, ], dtype=tf.complex64),
                        slice_position=tf.constant([0., 0., 0.], dtype=tf.float32),
                        slice_thickness=tf.constant(0.1, dtype=tf.float32),
                        slice_normal=tf.constant([0., 0., 1.], tf.float32),
                        slice_profile="Sinc1"
        )

        _ = module(traj_mod, init_pos, magnetization)
        self.assertTrue(np.allclose(np.abs(_)[0, 0:2], 1.))

        module = cmrsim.bloch.InstantaneousPulse(
                        complex_flip_angle=tf.constant([np.pi/2, ], dtype=tf.complex64),
                        slice_position=tf.constant([0., 0., 0.], dtype=tf.float32),
                        slice_thickness=tf.constant(0.1, dtype=tf.float32),
                        slice_normal=tf.constant([0., 0., 1.], tf.float32),
                        slice_profile=None
        )
        _ = module(traj_mod, init_pos, magnetization)
        self.assertTrue(np.allclose(np.abs(_)[0, 0:2], 1.))

