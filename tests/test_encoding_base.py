import unittest
import itertools

import tensorflow as tf
import numpy as np
from pint import Quantity

from cmrsim.analytic.encoding import BaseSampling, EPI


class TestInputShapes(unittest.TestCase):
    def setUp(self) -> None:
        n_sampling_point = 49

        class DummySampling(BaseSampling):
            def _calculate_trajectory(self):
                vecs = tf.random.uniform((n_sampling_point, 3))
                vecs = vecs / tf.linalg.norm(vecs, axis=1, keepdims=True)
                times = tf.zeros((n_sampling_point,))
                return vecs, times

        self.n_sampling_points = n_sampling_point
        self.module = DummySampling(absolute_noise_std=-1., )

    def test_orientation_matrix(self):
        slice_normal = np.array([0., 0., 1.])
        readout_dir = np.array([0., 1., 0.])
        slice_position = Quantity([0., 0., 0.], "mm")
        self.module.set_orientation_matrix(slice_position, slice_normal, readout_dir)

        m_transverse = tf.ones((100, 2, 1), dtype=tf.complex64)
        r_vectors = tf.random.normal((100, 2, 1, 3))
        k = self.module(m_transverse, r_vectors)


    def test_correct_input_shapes(self):
        """ Tests correct possible combinations of input shapes for positional vectors
        and transverse magnetization. Test fails if the shape of the k-space array does
        not match the target.
        """
        actual_number_of_contrasts = 10
        n_voxels = 1000
        shape_list = itertools.product([1, actual_number_of_contrasts], [1, self.n_sampling_points])
        for reps, timings in shape_list:
            with self.subTest():
                target_shape = (actual_number_of_contrasts, self.n_sampling_points)
                m_transverse = tf.ones((n_voxels, actual_number_of_contrasts, timings),
                                       dtype=tf.complex64)
                r_vectors = tf.random.normal((n_voxels, reps, timings, 3))
                k = self.module(m_transverse, r_vectors)
                self.assertTrue(all(i == j for i, j in zip(target_shape, tuple(k.shape))))


class TestValues(unittest.TestCase):
    def test_dc_input(self):
        m0 = tf.ones((1, 1, 1), dtype=np.complex64)
        r = tf.zeros((1, 1, 1, 3), dtype=tf.float32)

        encoding_module = EPI((1., 1.), sampling_matrix_size=(11, 10), absolute_noise_std=0.,
                              bandwidth_per_pixel=100., acquisition_start=1., blip_duration=0., )
        constant_k_space = encoding_module(m0, r)
        self.assertTrue(np.allclose(0., np.abs(constant_k_space - 1.)))

