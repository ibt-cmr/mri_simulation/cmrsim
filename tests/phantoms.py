__all__ = ["define_shepplogan", "define_cylinder_phantom", "dict_to_base", "dict_to_bloch"]

import unittest
from collections import OrderedDict
import os

import cmrsim
import numpy as np

import cmrsim.datasets
import cmrsim.utils
import phantominator


def dict_to_base(phantom_dict: dict):
    key1 = [k for k in phantom_dict.keys() if k != "r_vectors"]
    phantom = OrderedDict([(k, phantom_dict[k].reshape(-1, 1, 1)) for k in key1])
    phantom["r_vectors"] = phantom_dict["r_vectors"].reshape(-1, 1, 1, 3)
    return cmrsim.datasets.AnalyticDataset(phantom, filter_inputs=True)


def dict_to_bloch(phantom_dict: dict, n_time_steps: int):
    phantom = OrderedDict([])
    phantom["magnetization"] = (phantom_dict["M0"].reshape(-1, 1) *
                                np.array([[0., 0., 1.]])).astype(np.complex64)
    r_init = phantom_dict["r_vectors"].reshape(-1, 1, 3)
    phantom["trajectories"] = np.tile(r_init, [1, n_time_steps, 1])
    phantom["M0"] = np.abs(phantom_dict["M0"].reshape(-1, )).astype(np.float32)
    for k in phantom_dict.keys():
        if k != "r_vectors" and k != "M0":
            phantom[k] = phantom_dict[k].reshape(-1, )
    return cmrsim.datasets.BlochDataset(phantom, filter_inputs=True)


def define_shepplogan(fov=(0.3, 0.3), map_size=(300, 250)) -> (OrderedDict, np.ndarray):
    """ Constructs a shepp-logan phantom with correct coordinate definition and wraps it with
    a cmrsim Basedataset. The reference returned along with the dataset should be oriented
    the same as the simulation result when using imshow.
    :return: phantom_dict, m0ref, coordinates
     """
    M0, T1, T2 = phantominator.shepp_logan((*map_size, 5), MR=True)
    M0, T1, T2 = [t.transpose(1, 0, 2) for t in [M0, T1, T2]]
    r_vectors = cmrsim.utils.coordinates.get_static_2d_centered_coordinates(
                                                            map_size, fov).numpy()

    r_vectors = r_vectors.reshape(-1, 3)

    phantom = OrderedDict([("M0", M0[..., 2].astype(np.complex64)),
                           ("r_vectors", r_vectors.astype(np.float32)),
                           ("T1", T1[..., 2].astype(np.float32) * 1000),
                           ("T2", T2[..., 2].astype(np.float32) * 1000)])
    return phantom, M0[..., 2], r_vectors


def define_cylinder_phantom(fov=(0.3, 0.3), map_size=(300, 250), background_value=30) \
                        -> (OrderedDict, np.ndarray, np.ndarray):
    """ Helper Function to generate a 2D grid containing 3 circles with different diameters
    and different magnetization and relaxation times.

    :param fov: Expected field of view in meter
    :param map_size: array shape that the FOV is gridded
    :return: phantom_dict, m0ref, coordinates
    """
    xx, yy = np.meshgrid(*[range(i) for i in map_size], indexing='xy')
    circle_midpoints = np.array(map_size)[np.newaxis, :] * np.array(
        [[1 / 3, 1 / 3], [2 / 3, 1 / 2], [1 / 4, 2 / 3]])
    circle_radii = (20, 25, 15)
    rrs = [np.sqrt((xx - x0) ** 2 + (yy - y0) ** 2) for (x0, y0) in
           circle_midpoints.astype(int)]
    binarys = [np.where(r < cr, np.ones_like(r) * (i + 1), np.zeros_like(r))
               for i, r, cr in zip(range(len(rrs)), rrs, circle_radii)]
    class_map = np.sum(np.stack(binarys, axis=0), axis=0).astype(np.int64)

    coordinates = cmrsim.utils.coordinates.get_static_2d_centered_coordinates(
                                                        map_size=map_size,
                                                        object_dimensions=fov).numpy()
    coordinates = np.reshape(coordinates, (-1, 3))
    bg, c1, c2, c3 = [np.where(class_map == i) for i in range(4)]
    m0 = np.ones_like(class_map) * background_value
    m0[c1] = 100
    m0[c2] = 80
    m0[c3] = 50
    t1 = np.ones_like(class_map) * 500
    t2 = np.ones_like(class_map) * 50
    t2star = np.ones_like(class_map) * 35
    phantom_dictionary = OrderedDict([
        ('M0', m0.astype(np.complex64) / 4),
        ('r_vectors', coordinates.astype(np.float32)),
        ('T1', t1.astype(np.float32)),
        ('T2', t2.astype(np.float32)),
        ('T2star', t2star.astype(np.float32))])
    return phantom_dictionary, m0.astype(np.float32), coordinates
