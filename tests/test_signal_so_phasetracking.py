import unittest
import os

import numpy as np
import matplotlib.pyplot as plt

from cmrsim.analytic.contrast import phase_tracking
import cmrseq
from pint import Quantity

plt.close("all")


class TestDiffusionWaveFormTracking(unittest.TestCase):
    """ The Wave form in 'resources/  ' contains the definition of a M012 diffusion encoding
    waveform on a time-grid (0, 115) ms,  with a b-value of 100 s/mm^2 and a direction (0, 1, 0)
    therefor only motion in y-direction with non-zero jerk (or higher order moments) result in a
    non-zero phase:

    - For static, constant velocity and constant accellaration phantom the final phase distribution
    must only contain 0 values.
    - For a sinosoidal motion of particles in a ring (contraction like) with half a period over
    the wave form it must differ from 0.
    """
    def setUp(self) -> None:
        self.system_specs = cmrseq.SystemSpec(max_grad=Quantity(80., "mT/m"),
                                              max_slew=Quantity(80., "mT/m/ms"),
                                              grad_raster_time=Quantity(0.01, "ms"))
        seq = cmrseq.seqdefs.diffusion.m012(self.system_specs, lambda_=Quantity(5.6, "ms"),
                                            zeta=Quantity(0.9, "ms"),
                                            bvalue=Quantity(100., "s/mm**2"),
                                            direction=np.array([0., 1., 0.]))
        t, wf = seq.gradients_to_grid()
        wf = np.concatenate([t[np.newaxis], wf], axis=0).T
        self.mod = phase_tracking.PhaseTracking(expand_repetitions=True,
                                                wave_form=wf[np.newaxis].astype(np.float32),
                                                gamma=self.system_specs.gamma.m_as("MHz/T")*2*np.pi)
        self.n_timesteps = wf.shape[0]

    @staticmethod
    def _get_ring_coordinates():
        xx, yy, zz = np.meshgrid(range(250), range(250), 1)
        coords = (np.stack([xx, yy, zz], axis=-1) - np.array([[125., 125, 0]])).reshape(-1, 3)/ 150.
        r = np.linalg.norm(coords[..., 0:2], axis=-1)
        radial_coords = coords[np.where(np.logical_and(r > 0.4, r < 0.5))] * 0.05
        return radial_coords.astype(np.float32)

    def test_static_phantom(self):
        rad_cords = self._get_ring_coordinates()
        r = rad_cords.reshape(-1, 1, 1, 3)

        # static trajectory
        trajectory = np.tile(r.reshape(-1, 1, 1, 1, 3), [1, 1, 1, self.n_timesteps, 1])

        signal_tensor = np.ones((rad_cords.shape[0], 1, 1), dtype=np.complex64)
        signal_tensor = self.mod(signal_tensor, trajectory)
        phase = np.angle(signal_tensor)
        self.assertTrue(np.allclose(phase, 0., atol=np.pi/1000),
                        msg="Sum of all phases is greater than numerical precision "
                            "tolerance for static phantom")

    def test_constant_velocity(self):
        rad_cords = self._get_ring_coordinates()
        r = rad_cords.reshape(-1, 1, 3, 1)

        # static trajectory
        trajectory = np.tile(r.reshape(r.shape[0], 1, 1, 1, 3), [1, 1, 1, self.n_timesteps, 1])

        t = self.mod.wave_form[..., 0].numpy()
        delta_y = t.reshape([1, 1, 1, -1]) / 300.
        trajectory[..., :, 1] += np.tile(delta_y, [r.shape[0], 1, 1, 1])

        signal_tensor = np.ones((rad_cords.shape[0], 1, 1), dtype=np.complex64)
        signal_tensor = self.mod(signal_tensor, trajectory)
        phase = np.angle(signal_tensor)
        self.assertAlmostEqual(np.sum(np.abs(phase))/r.shape[0], 0., delta=np.pi/100,
                               msg="Not all phases are zero for constant velocity phantom")

    def test_constant_acceleration(self):
        rad_cords = self._get_ring_coordinates()
        r = rad_cords.reshape(-1, 1, 3, 1)

        # static trajectory
        trajectory = np.tile(r.reshape(r.shape[0], 1, 1, 1, 3), [1, 1, 1, self.n_timesteps, 1])

        t = self.mod.wave_form[..., 0].numpy()
        delta_y = 1e-4/2 * t**2
        trajectory[..., :, 1] += np.tile(delta_y, [r.shape[0], 1, 1, 1])

        signal_tensor = np.ones((rad_cords.shape[0], 1, 1), dtype=np.complex64)
        signal_tensor = self.mod(signal_tensor, trajectory)
        phase = np.angle(signal_tensor)
        self.assertTrue(np.allclose(np.abs(phase), 0., atol=np.pi/100),
                        msg="Not all phases are zero for constant acceleration phantom")

    def test_sinosodial_motion(self):
        rad_cords = self._get_ring_coordinates()

        # static trajectory
        fine_time_scale = self.mod.wave_form[0, ..., 0].numpy()

        trajectory = np.tile(rad_cords[np.newaxis], [fine_time_scale.shape[0], 1, 1])

        # Contraction by 0.1 r
        trajectory[:, :, 0:2] -= trajectory[0:1, :, 0:2] * np.sin(fine_time_scale.reshape(-1, 1, 1)
                                                                 / fine_time_scale[-1] * 3/2 * np.pi)

        s = np.ones((rad_cords.shape[0], 1, 1), dtype=np.float32)
        signal_tensor = s + 1j * np.zeros_like(s)
        signal_tensor = self.mod(signal_tensor, trajectory.reshape([-1, 1, 1, self.n_timesteps, 3]))
        phase = np.angle(signal_tensor)

        # Plot phase distribution
        output_dir = f"{os.path.dirname(__file__)}/test_output/signal"
        os.makedirs(output_dir, exist_ok=True)
        f, (a, ahis) = plt.subplots(1, 2)
        a.plot(fine_time_scale, trajectory[:, 0::100, 1], label="$r(t)$", alpha=0.6)

        f.set_size_inches(12, 6)
        [a.plot(fine_time_scale, self.mod.wave_form.read_value()[0, ..., i] / 1000., label="$G_{{i}}$")
         for i in range(1, 4)]

        # a.legend(), a.grid(True), a.set_ylim([-0.1, 0.1]), a.set_xlim([0., fine_time_scale[-1]])
        a.set_title("Radial trajectory per material point")
        ahis.set_title("Final Phase distribution")
        ahis.hist(phase.flatten(), bins=100)
        f.savefig(f"{output_dir}/sinosoidal_phasetracking.png")

        self.assertGreater(np.sum(np.abs(phase)), 1000.,
                           msg="All phases are zero for higher order phantom")
