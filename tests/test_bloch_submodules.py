import unittest

import tensorflow as tf
tf.config.run_functions_eagerly(True)

import cmrsim


class TestConcomittantFieldSubmodule(unittest.TestCase):
    def setUp(self) -> None:
        self.mod = cmrsim.bloch.submodules.ConcomitantFields(gamma=42., b0=1)

    def test_input_shape(self):
        n_reps, n_batch, n_timesteps = 2, 10, 1
        dummy_wf = tf.ones([n_reps, n_timesteps, 3], dtype=tf.float32)
        dummy_r = tf.ones([1, n_batch, n_timesteps, 3], dtype=tf.float32)
        dt = tf.constant(0.01, tf.float32)
        phi = self.mod(gradient_wave_form=dummy_wf, trajectories=dummy_r, dt=dt)
        print(phi.shape)


class TestT2starSubmodule(unittest.TestCase):
    def setUp(self) -> None:
        self.mod = cmrsim.bloch.submodules.T2starDistributionModel()

    def test_input_shape(self):
        with self.subTest("Valid shape 1 dims"):
            n_batch, n_timesteps = 10, 1
            dummy_omega = tf.ones([n_batch, ], dtype=tf.float32)
            dummy_r = tf.ones([1, n_batch, n_timesteps, 3], dtype=tf.float32)
            dt = tf.constant(0.01, tf.float32)
            phi = self.mod(omega_t2s=dummy_omega, trajectories=dummy_r, dt=dt)
            self.assertTrue(len(phi.shape) == 3)
            self.assertTrue(all([i == j for i, j in
                                 zip(phi.shape, (1, n_batch, n_timesteps))]))
            

class TestOffResonanceSubmodule(unittest.TestCase):
    def setUp(self) -> None:
        self.mod = cmrsim.bloch.submodules.OffResonance(gamma=42)

    def test_input_shape(self):
        with self.subTest("Valid shape 3 dims"):
            n_reps, n_batch, n_timesteps, n_comp = 2, 10, 1, 1
            dummy_offres = tf.ones([n_batch, n_batch, n_timesteps, n_comp], dtype=tf.float32)
            dt = tf.constant(0.01, tf.float32)
            phi = self.mod(off_res=dummy_offres, dt=dt)
            self.assertTrue(len(phi.shape) == 3)

        with self.subTest("Valid shape 2 dims"):
            """ Checks if expansion of repetition axis is performed correctly"""
            n_batch, n_steps, n_comp = 20, 1, 1
            dummy_offres = tf.random.uniform([n_batch, n_steps, n_comp], dtype=tf.float32)
            dt = tf.constant(0.01, tf.float32)
            phi = self.mod(off_res=dummy_offres, dt=dt)
            self.assertTrue(len(phi.shape) == 3)

        with self.subTest("Valid shape 1 dims"):
            """ Checks if expansion of repetition axis is performed correctly"""
            n_batch, n_comp = 20 ,1
            dummy_offres = tf.random.uniform([n_batch, n_comp], dtype=tf.float32)
            dt = tf.constant(0.01, tf.float32)
            phi = self.mod(off_res=dummy_offres, dt=dt)
            self.assertTrue(len(phi.shape) == 3)
