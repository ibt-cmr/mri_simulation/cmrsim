# CMRsim Containers

The files included in this provide the definitions to build Docker-Images as well as 
Apptainer / Singularity image files to run CMRsim in containerized environments. 

For each of the containerization frameworks three images are provided:

1. Minimal CPU installation, used for prototyping and unittesting
2. GPU installation, used to run full simulation scripts
3. GPU installation with JupyterLab included to develop interactively

These images are meant to be built on every release (minor version changes minimum), therefore 
developmental increments of the cmrsim package (X.Y.devZ) are not installed automatically.